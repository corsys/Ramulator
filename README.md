# Multi-tier Ramulator: A DRAM Simulator for heterogeneous memory systems

Multi-tier Ramulator is an extension of Ramulator \[1\] that simulates 
heterogeneous memory systmes and supports a wide array of commercial, 
as well as academic, DRAM standards:

- DDR3 (2007), DDR4 (2012)
- LPDDR3 (2012), LPDDR4 (2014)
- GDDR5 (2009)
- WIO (2011), WIO2 (2014)
- HBM (2013)
- SALP \[2\]
- TL-DRAM \[3\]
- RowClone \[4\]
- DSARP \[5\]

[\[1\] Kim et al. *Ramulator: A Fast and Extensible DRAM Simulator.* IEEE CAL
2015.](https://users.ece.cmu.edu/~omutlu/pub/ramulator_dram_simulator-ieee-cal15.pdf)  
[\[2\] Kim et al. *A Case for Exploiting Subarray-Level Parallelism (SALP) in
DRAM.* ISCA 2012.](https://users.ece.cmu.edu/~omutlu/pub/salp-dram_isca12.pdf)  
[\[3\] Lee et al. *Tiered-Latency DRAM: A Low Latency and Low Cost DRAM
Architecture.* HPCA 2013.](https://users.ece.cmu.edu/~omutlu/pub/tldram_hpca13.pdf)  
[\[4\] Seshadri et al. *RowClone: Fast and Energy-Efficient In-DRAM Bulk Data
Copy and Initialization.* MICRO
2013.](https://users.ece.cmu.edu/~omutlu/pub/rowclone_micro13.pdf)  
[\[5\] Chang et al. *Improving DRAM Performance by Parallelizing Refreshes with
Accesses.* HPCA 2014.](https://users.ece.cmu.edu/~omutlu/pub/dram-access-refresh-parallelization_hpca14.pdf)


## Usage

Multi-tier Ramulator currently only supports one usage mode.

1. **CPU Trace Driven:** Multi-tier Ramulator directly reads instruction traces from a 
  file, and simulates a simplified model of a "core" that generates memory 
  requests to the DRAM subsystem. Each line in the trace file represents a 
  memory request, and can have one of the following two formats.

  - `<num-cpuinst> <tier-req> <addr-req> <req-type>`: The first token 
        represents the number of CPU (i.e., non-memory) instructions before
        the memory request, and the second token is a request for the tier to
        be accessed on, the third token is the physical address being requested,
        the fourth token is the request type.
  - Request types can be one of eight different types:

  1. READ
  2. WRITE
  3. UNMAP
  4. PHASE\_START
  5. PHASE\_END
  6. INVALID\_HOT\_PAGES
  7. MIGRATE\_WITH\_FAULT
  8. MIGRATE\_SANS\_FAULT
 

Support for other modes coming in the future

## Getting Started

Multi-tier Ramulator requires a C++11 compiler (e.g., `clang++`, `g++-5`).

        $ cd ramulator
        $ make -j

        Usage: ./ramulator <tier1-configs-file> <tier2-configs-file> --mode=cpu \
        [--tier_limit limit] [--stats_1 <filename>] [--stats\_2 <filename>] \
        <trace-filename1> <trace-filename2>
        Example: ./ramulator primary-configs.cfg secondary-configs.cfg --mode=cpu cpu.trace
## Simulation Output

Multi-tier Ramulator will report a series of statistics for every run, which are written
to a file.  We have provided a series of gem5-compatible statistics classes in
`Statistics.h`.

**Memory Trace/CPU Trace Driven**: When run in memory trace driven or CPU trace
driven mode, Multi-tier Ramulator will write these statistics to a file.  By default, the
filename will be `<standard_name>.stats` (e.g., `DDR3.stats`).  You can write
the statistics file to a different filename by adding `--stats <filename>` to
the command line after the `--mode` switch (see examples above).

*NOTE: When creating your own stats objects, don't place them inside STL
containers that are automatically resized (e.g, vector).  Since these
containers copy on resize, you will end up with duplicate statistics printed
in the output file.*



### Debugging & Verification 

For debugging and verification purposes, Multi-tier Ramulator can print the trace of every
DRAM command it issues along with their address and timing information. To do
so, please turn on the `print_cmd_trace` variable in the configuration file.

