#include "Processor.h"
#include "Config.h"
#include "Controller.h"
#include "SpeedyController.h"
#include "Memory.h"
#include "DRAM.h"
#include "Statistics.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>
#include <functional>
#include <map>

/* Standards */
#include "Gem5Wrapper.h"
#include "DDR3.h"
#include "DDR4.h"
#include "DSARP.h"
#include "GDDR5.h"
#include "LPDDR3.h"
#include "LPDDR4.h"
#include "WideIO.h"
#include "WideIO2.h"
#include "HBM.h"
#include "SALP.h"
#include "ALDRAM.h"
#include "TLDRAM.h"
#include "Params.h"

using namespace std;
using namespace ramulator;

template<typename T1, typename T2>
void run_dramtrace(const Config& configs1, Memory<T1, Controller>& memory1, 
                   const Config& configs2, Memory<T2, Controller>& memory2,
                   const char* tracename) 
{
    /* initialize DRAM trace */
    Trace trace(tracename);

    /* run simulation */
    bool stall = false, end = false;
    int reads = 0, writes = 0, clks = 0;
    long addr = 0;
    Request::Type type = Request::Type::READ;
    MemType memtype = MemType::TIER1;
    map<int, int> latencies;
    auto read_complete = [&latencies](Request& r){latencies[r.depart - r.arrive]++;};

    Request req(addr, type, memtype, memtype, read_complete);

    while (!end || memory1.pending_requests() || memory2.pending_requests()){
        if (!end && !stall){
            end = !trace.get_dramtrace_request(addr, type, memtype);
        }

        if (!end){
            req.addr = addr;
            req.type = type;
            req.memtype = memtype;

            if (req.memtype == MemType::TIER1) {
                stall = !memory1.send(req);
            } else if (req.memtype == MemType::TIER2) {
                stall = !memory2.send(req);
            } else {
                cout << "invalid memory type: " << (int) req.memtype << "\n";
                return;
            }

            if (!stall){
                if (type == Request::Type::READ) reads++;
                else if (type == Request::Type::WRITE) writes++;
            }
        }
        
        if (req.memtype == MemType::TIER1) {
            memory1.tick();
        } else if (req.memtype == MemType::TIER2) {
            memory2.tick();
        } else {
           cout << "invalid memory type: " << (int) req.memtype << "\n";
           return;
        }

        clks ++;
        Stats::curTick++; // memory clock, global, for Statistics
    }
    // This a workaround for statistics set only initially lost in the end
    memory1.finish();
    memory2.finish();
    Stats::statlist1.printall();
    Stats::statlist2.printall();

}

template <typename T1, typename T2>
void run_cputrace(const Config& configs1, Memory<T1, Controller>& memory1,
                  const Config& configs2, Memory<T2, Controller>& memory2,
                  const std::vector<const char *>& files)
{
    int cpu_tick1 = configs1.get_cpu_tick();
    int mem_tick1 = configs1.get_mem_tick();
    auto send1 = bind(&Memory<T1, Controller>::send, &memory1, placeholders::_1);

    int cpu_tick2 = configs2.get_cpu_tick();
    int mem_tick2 = configs2.get_mem_tick();
    auto send2 = bind(&Memory<T2, Controller>::send, &memory2, placeholders::_1);
    
    Processor proc(configs1, configs2, files, send1, send2, memory1, memory2);
    for (long i = 0; ; i++) {
        proc.tick();
        Stats::curTick++; // processor clock, global, for Statistics
        if (i % cpu_tick1 == (cpu_tick1 - 1))
            for (int j = 0; j < mem_tick1; j++)
                memory1.tick();
        
        if (i % cpu_tick2 == (cpu_tick2 - 1))
            for (int j = 0; j < mem_tick2; j++)
                memory2.tick();
        
        if (configs1.calc_weighted_speedup() 
            || configs2.calc_weighted_speedup()) 
        {
            if (proc.has_reached_limit()) {
                break;
            }
        } else {
            if (configs1.is_early_exit() || configs2.is_early_exit()) {
                if (proc.finished())
                    break;
            } else {
                if (proc.finished() 
                    && (memory1.pending_requests() == 0)
                    && (memory2.pending_requests() == 0))
                {
                    break;
                }
            }
        }
    }

    // This a workaround for statistics set only initially lost in the end
    memory1.finish();
    memory2.finish();
    Stats::statlist1.printall();
    Stats::statlist2.printall();
}

template<typename T1, typename T2>
void start_run_helper(const Config& configs1, T1* spec1, 
                      const Config& configs2, T2* spec2,
                      const vector<const char*>& files) 
{
  // initiate controller and memory
  int C1 = configs1.get_channels(), R1 = configs1.get_ranks();
  Params* myp = Params::instance();
  // Check and Set channel, rank number
  spec1->set_channel_number(C1);
  spec1->set_rank_number(R1);
  std::vector<Controller<T1>*> ctrls1;
  for (int c = 0 ; c < C1 ; c++) {
    DRAM<T1>* channel = new DRAM<T1>(spec1, T1::Level::Channel);
    channel->id = c;
    channel->regStats("", configs1);
    Controller<T1>* ctrl = new Controller<T1>(configs1, channel);
    ctrls1.push_back(ctrl);
  }
  Memory<T1, Controller> memory1(configs1, ctrls1, myp->t1_limit);
  
  // initiate controller and memory
  int C2 = configs2.get_channels(), R2 = configs2.get_ranks();
  // Check and Set channel, rank number
  spec2->set_channel_number(C2);
  spec2->set_rank_number(R2);
  std::vector<Controller<T2>*> ctrls2;
  for (int c = 0 ; c < C2 ; c++) {
    DRAM<T2>* channel = new DRAM<T2>(spec2, T2::Level::Channel);
    channel->id = c;
    channel->regStats("", configs2);
    Controller<T2>* ctrl = new Controller<T2>(configs2, channel);
    ctrls2.push_back(ctrl);
  }
  Memory<T2, Controller> memory2(configs2, ctrls2);

  assert(files.size() != 0);
  if (configs1["trace_type"] == "CPU") {
    run_cputrace(configs1, memory1, configs2, memory2, files);
  } else if (configs1["trace_type"] == "DRAM") {
    run_dramtrace(configs1, memory1, configs2, memory2, files[0]);
  }
}

template <typename T>
void start_run(const Config& configs1, T* spec1,
               const Config& configs2, const std::string& standard2,
               std::vector<const char*> files) 
{
  if (standard2 == "DDR3") {
    DDR3* ddr3 = new DDR3(configs2["org"], configs2["speed"]);
    start_run_helper(configs1, spec1, configs2, ddr3, files);
  } else if (standard2 == "DDR4") {
    DDR4* ddr4 = new DDR4(configs2["org"], configs2["speed"]);
    start_run_helper(configs1, spec1, configs2, ddr4, files);
  } else if (standard2 == "SALP-MASA") {
    SALP* salp8 = new SALP(configs2["org"], configs2["speed"], "SALP-MASA", configs1.get_subarrays());
    start_run_helper(configs1, spec1, configs2, salp8, files);
  } else if (standard2 == "LPDDR3") {
    LPDDR3* lpddr3 = new LPDDR3(configs2["org"], configs2["speed"]);
    start_run_helper(configs1, spec1, configs2, lpddr3, files);
  } else if (standard2 == "LPDDR4") {
    // total cap: 2GB, 1/2 of others
    LPDDR4* lpddr4 = new LPDDR4(configs2["org"], configs2["speed"]);
    start_run_helper(configs1, spec1, configs2, lpddr4, files);
  } else if (standard2 == "GDDR5") {
    GDDR5* gddr5 = new GDDR5(configs2["org"], configs2["speed"]);
    start_run_helper(configs1, spec1, configs2, gddr5, files);
  } else if (standard2 == "HBM") {
    HBM* hbm = new HBM(configs2["org"], configs2["speed"]);
    start_run_helper(configs1, spec1, configs2, hbm, files);
  } else if (standard2 == "WideIO") {
    // total cap: 1GB, 1/4 of others
    WideIO* wio = new WideIO(configs2["org"], configs2["speed"]);
    start_run_helper(configs1, spec1, configs2, wio, files);
  } else if (standard2 == "WideIO2") {
    // total cap: 2GB, 1/2 of others
    WideIO2* wio2 = new WideIO2(configs2["org"], configs2["speed"], configs1.get_channels());
    wio2->channel_width *= 2;
    start_run_helper(configs1, spec1, configs2, wio2, files);
  }
  // Various refresh mechanisms
    else if (standard2 == "DSARP") {
    DSARP* dsddr3_dsarp = new DSARP(configs2["org"], configs2["speed"], DSARP::Type::DSARP, configs1.get_subarrays());
    start_run_helper(configs1, spec1, configs2, dsddr3_dsarp, files);
  } else if (standard2 == "ALDRAM") {
    ALDRAM* aldram = new ALDRAM(configs2["org"], configs2["speed"]);
    start_run_helper(configs1, spec1, configs2, aldram, files);
  } else if (standard2 == "TLDRAM") {
    TLDRAM* tldram = new TLDRAM(configs2["org"], configs2["speed"], configs1.get_subarrays());
    start_run_helper(configs1, spec1, configs2, tldram, files);
  }
}

int main(int argc, char* const argv[])
{

    Params::create_instance(argc, argv);
    Params *myp = Params::instance();


    Config configs1(myp->files.front(), MemType::TIER1);
    myp->files.erase(myp->files.begin());
    Config configs2(myp->files.front(), MemType::TIER2);
    myp->files.erase(myp->files.begin());

    const std::string& standard1 = configs1["standard"];
    const std::string& standard2 = configs2["standard"];
    assert(standard1 != "" || "DRAM standard should be specified.");
    assert(standard2 != "" || "DRAM standard should be specified.");

    const char *trace_type = myp->mode.c_str();
    if (strcmp(trace_type, "cpu") == 0) {
      configs1.add("trace_type", "CPU");
      configs2.add("trace_type", "CPU");
    } else if (strcmp(trace_type, "dram") == 0) {
      configs1.add("trace_type", "DRAM");
      configs2.add("trace_type", "DRAM");
    } else {
      printf("invalid trace type: %s\n", trace_type);
      assert(false);
    }

    string stats_out1, stats_out2;
    if (myp->s1_fn !="") {
      Stats::statlist1.output(myp->s1_fn);
      stats_out1 = myp->s1_fn;
    } else {
      Stats::statlist1.output(standard1+".stats");
      stats_out1 = standard1 + string(".stats");
    }
    if (myp->s2_fn !="") {
      Stats::statlist2.output(myp->s2_fn);
      stats_out2 = myp->s2_fn;
    } else {
      Stats::statlist2.output(standard2+".stats");
      stats_out2 = standard2 + string(".stats");
    }
    
    std::vector<const char*> files = myp->files;
    configs1.set_core_num(myp->files.size());
    configs2.set_core_num(myp->files.size());

    if (standard1 == "DDR3") {
      DDR3* ddr3 = new DDR3(configs1["org"], configs1["speed"]);
      start_run(configs1, ddr3, configs2, standard2, files);
    } else if (standard1 == "DDR4") {
      DDR4* ddr4 = new DDR4(configs1["org"], configs1["speed"]);
      start_run(configs1, ddr4, configs2, standard2, files);
    } else if (standard1 == "SALP-MASA") {
      SALP* salp8 = new SALP(configs1["org"], configs1["speed"], "SALP-MASA", configs1.get_subarrays());
      start_run(configs1, salp8, configs2, standard2, files);
    } else if (standard1 == "LPDDR3") {
      LPDDR3* lpddr3 = new LPDDR3(configs1["org"], configs1["speed"]);
      start_run(configs1, lpddr3, configs2, standard2, files);
    } else if (standard1 == "LPDDR4") {
      // total cap: 2GB, 1/2 of others
      LPDDR4* lpddr4 = new LPDDR4(configs1["org"], configs1["speed"]);
      start_run(configs1, lpddr4, configs2, standard2, files);
    } else if (standard1 == "GDDR5") {
      GDDR5* gddr5 = new GDDR5(configs1["org"], configs1["speed"]);
      start_run(configs1, gddr5, configs2, standard2, files);
    } else if (standard1 == "HBM") {
      HBM* hbm = new HBM(configs1["org"], configs1["speed"]);
      start_run(configs1, hbm, configs2, standard2, files);
    } else if (standard1 == "WideIO") {
      // total cap: 1GB, 1/4 of others
      WideIO* wio = new WideIO(configs1["org"], configs1["speed"]);
      start_run(configs1, wio, configs2, standard2, files);
    } else if (standard1 == "WideIO2") {
      // total cap: 2GB, 1/2 of others
      WideIO2* wio2 = new WideIO2(configs1["org"], configs1["speed"], configs1.get_channels());
      wio2->channel_width *= 2;
      start_run(configs1, wio2, configs2, standard2, files);
    }
    // Various refresh mechanisms
      else if (standard1 == "DSARP") {
      DSARP* dsddr3_dsarp = new DSARP(configs1["org"], configs1["speed"], DSARP::Type::DSARP, configs1.get_subarrays());
      start_run(configs1, dsddr3_dsarp, configs2, standard2, files);
    } else if (standard1 == "ALDRAM") {
      ALDRAM* aldram = new ALDRAM(configs1["org"], configs1["speed"]);
      start_run(configs1, aldram, configs2, standard2, files);
    } else if (standard1 == "TLDRAM") {
      TLDRAM* tldram = new TLDRAM(configs1["org"], configs1["speed"], configs1.get_subarrays());
      start_run(configs1, tldram, configs2, standard2, files);
    }

    printf("Simulation done. Statistics written to %s\n", stats_out1.c_str());
    printf("Simulation done. Statistics written to %s\n", stats_out2.c_str());

    return 0;
}
