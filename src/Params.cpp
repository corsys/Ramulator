#include <getopt.h>
#include <stdio.h>
#include <iostream>
#include "Params.h"

using namespace ramulator;
void Params::create_instance(int argc, char* const argv[])
{
      if(!s_instance)
        s_instance = new Params(argc, argv);
}

Params* Params::instance()
{
      return s_instance;
}

Params::Params(int argc, char *const argv[])
{
  int index;
  int iarg = 0;
  bool limit_set = false;
  bool theta_set = false;

  opterr=1;

  while((iarg = getopt_long(argc, argv, shortstr.c_str(), longopts, &index)) != -1)
  {
    switch(iarg){
      case 'h':
        printf(usage_message, argv[0], argv[0]);
        exit(0);
        break;
      case 't':
        t1_limit = atoi(optarg);
        limit_set = true;
        break;
      case 'a':
        initial_theta = atoi(optarg);
        theta_set = true;
        break;
      case 'm':
        mode = optarg;
        break;
      case 'S':
        s1_fn = optarg;
        break;
      case 's':
        s2_fn = optarg;
        break;
      case 'l':
        epoch_length = atol(optarg);
        break;
      case 'e':
        epoch_policy = optarg;
        break;
      case '?':
        break;
      case 1:
        files.push_back(optarg);
        break;
      default:
        //std::cout << "Hit Default: " << iarg  << std::endl;
        break;
    }
  }

  if (!limit_set) {
    t1_limit = UINT_MAX;
  }
  if (!theta_set) {
    initial_theta = 32;
  }
}

Params *Params::s_instance = 0;
int Params::t1_limit = 0;
int Params::initial_theta = 0;
int Params::fault_penalty = false;
int Params::migrate_penalty = false;
int Params::shootdown_penalty = false;
int Params::phase_free = false;
int Params::phase_change_free = false;
int Params::history_guide = false;
int Params::random_guide = false;
long Params::epoch_length=0;
std::string Params::epoch_policy="";
std::string Params::mode="";
std::string Params::s1_fn="";
std::string Params::s2_fn="";
std::vector <const char *> Params::files = {};
