#ifndef __EPOCH_H
#define __EPOCH_H
#include <vector>
#include <iostream>
#include <functional>
#include <cmath>
#include <cassert>
#include <tuple>
#include <set>
#include <fstream>
#include <sstream>
#include "Memory.h"
#include "Statistics.h"

using namespace std;
namespace ramulator
{
  class Core;

  enum EpochPolicy {
    EPOCH_INVALID = -1,
    EPOCH_FIRST_TOUCH,
    EPOCH_FTHP_SORT,
    EPOCH_FTHP_AUTO_ADJ,
    EPOCH_GUIDE,
    NR_EPOCH_POLICIES
  };

  class EpochBase{
    public:
      EpochBase() {}
      virtual ~EpochBase() {}
      virtual void tick() = 0;
  };

  class Epoch : public EpochBase
  {
    public:
      long length = 0;
      unsigned long limit;
      EpochPolicy policy;

      Epoch(long length, long limit, EpochPolicy policy,
            double theta, MemoryBase& memory1, MemoryBase& memory2);

      bool is_pending_invalidate() { return pending_invalidate; }
      bool is_pending_shootdown()  { return pending_shootdown;  }
      bool is_pending_preload()    { return pending_preload;    }
      bool has_pending_tick()      { return pending_tick;       }

      void clear_pending_shootdown() { pending_shootdown = false; }
      void clear_pending_preload()   { pending_preload = false;   }
      void set_pending_tick(bool t)  { pending_tick = t;          }

      bool need_epoch_invalidate() {
        return true;
      }

      bool need_epoch_shootdown() {
        return !(need_epoch_preload());
      }

      bool need_epoch_preload() {
        switch (policy) {
          case EPOCH_FIRST_TOUCH:   return false;
          case EPOCH_FTHP_SORT:     return true;
          case EPOCH_FTHP_AUTO_ADJ: return true;
          case EPOCH_GUIDE:         return true;
          default:                  return false; // should not reach here
        };
      }

      bool try_invalidate();
      void tick();
      void load_hot_pages(Core& core);

      static EpochPolicy get_epoch_policy(string pol);

    protected:
      bool pending_invalidate;
      bool pending_shootdown;
      bool pending_preload;
      bool pending_tick;
      double theta;
      multimap <long, long, greater<long>> sorted_pages;

      MemoryBase& memory1;
      MemoryBase& memory2;

      ScalarStat epoch_ticks;
      VectorStat epoch_ticks_phase;
  };
} /*namespace ramulator*/

#endif /*__EPOCH_H*/
