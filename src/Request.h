#ifndef __REQUEST_H
#define __REQUEST_H

#include <vector>
#include <functional>

using namespace std;

namespace ramulator
{

enum MemType
{
  NONE = -1,
  TIER1,
  TIER2,
  NR_MEM_TIERS,
  UNCLASSIFIED = NR_MEM_TIERS,
  NR_MEM_TYPES,
  BOTH,
};

enum MemStatus
{
  VALID,
  INVALID,
  INVALID_HOT,
  RESERVED,
  ZOMBIE,
  EMPTY_RESERVED,
  NR_MEM_STATUSES,
};

class Request
{
public:
    bool is_first_command;
    long addr;
    // long addr_row;
    vector<int> addr_vec;
    // specify which core this request sent from, for virtual address translation
    int coreid;

    enum class Type
    {
        READ,
        WRITE,
        REFRESH,
        POWERDOWN,
        SELFREFRESH,
        EXTENSION,
        UNMAP,
        PHASE_START,
        PHASE_END,
        INVALID_HOT_PAGE,
        MIGRATE_WITH_FAULT,
        MIGRATE_SANS_FAULT,
        PURGE,
        SKIP,
        MAX,
    } type;

    MemType memtype, req_memtype;
    bool fault;

    long arrive = -1;
    long depart;
    function<void(Request&)> callback; // call back with more info

    Request(long addr, Type type, MemType memtype, MemType req_memtype=MemType::NONE, int coreid = 0)
        : is_first_command(true), addr(addr), coreid(coreid), type(type),
          memtype(memtype), req_memtype(req_memtype), fault(false), callback([](Request& req){}) {}

    Request(long addr, Type type, MemType memtype, MemType req_memtype, 
            function<void(Request&)> callback, int coreid = 0)
        : is_first_command(true), addr(addr), coreid(coreid), type(type), 
          memtype(memtype), req_memtype(req_memtype), fault(false), callback(callback) {}
    
    Request(vector<int>& addr_vec, Type type, MemType memtype, MemType req_memtype, 
            function<void(Request&)> callback, int coreid = 0)
        : is_first_command(true), addr_vec(addr_vec), coreid(coreid), type(type), 
          memtype(memtype), req_memtype(req_memtype), fault(false), callback(callback) {}


    Request()
        : is_first_command(true), coreid(0), fault(false) {}
};

} /*namespace ramulator*/

#endif /*__REQUEST_H*/

