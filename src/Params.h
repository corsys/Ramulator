#include <getopt.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <climits>

#ifndef PARAM_H
#define PARAM_H

#define no_argument 0
#define required_argument 1
#define optional_argument 2
#define NUM_OPTIONS  16
namespace ramulator {
class Params {
  public:
    static void create_instance(int argc, char* const argv[]);
    static Params *instance();
    static int t1_limit;
    static int initial_theta;
    static int fault_penalty;
    static int migrate_penalty;
    static int shootdown_penalty;
    static int phase_free;
    static int phase_change_free;
    static int history_guide;
    static int random_guide;
    static long epoch_length;
    static std::string epoch_policy;
    static std::string mode;
    static std::string s1_fn;
    static std::string s2_fn;
    static std::vector <const char *> files;

  private:
    Params(int argc, char *const argv[]);
    const char * usage_message = "Usage: %s <configs-file> <configs-file> --mode=cpu,dram [--tier_limit limit] [--stats_1 <filename>] [--stats_2 <filename>] <trace-filename1> <trace-filename2>\n \
                                  Example: %s primary-configs.cfg secondary-configs.cfg --mode=cpu cpu.trace cpu.trace\n";
    const std::string shortstr = "-ht:a:m:S:s:l:e:fgdpciur";
    static Params *s_instance;
    const struct option longopts[NUM_OPTIONS] = 
    { 
      {"help",          no_argument,        0, 'h'},
      {"mode",          required_argument,  0, 'm'},
      {"t1_limit",      required_argument,  0, 't'},
      {"init_theta",    required_argument,  0, 'a'},
      {"stats_1",       required_argument,  0, 'S'},
      {"stats_2",       required_argument,  0, 's'},
      {"epoch_length",  required_argument,  0, 'l'},
      {"epoch_policy",  required_argument,  0, 'e'},
      {"fault_penalty",       no_argument,  &fault_penalty,     1},
      {"migrate_penalty",     no_argument,  &migrate_penalty,   1},
      {"shootdown_penalty",   no_argument,  &shootdown_penalty, 1},
      {"phase_free",          no_argument,  &phase_free,        1},
      {"phase_change_free",   no_argument,  &phase_change_free, 1},
      {"history_guide",       no_argument,  &history_guide,     1},
      {"random_guide",        no_argument,  &random_guide,      1},
      {0,0,0,0}
    };
};
}

#endif
