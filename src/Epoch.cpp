#include "Epoch.h"
#include "Processor.h"
#include "Params.h"

using namespace std;
using namespace ramulator;

EpochPolicy Epoch::get_epoch_policy(string pol) {
  if ( pol == "first_touch" ) {
    return EPOCH_FIRST_TOUCH;
  } else if ( pol == "fthp_sort" ) {
    return EPOCH_FTHP_SORT;
  } else if ( pol == "fthp_auto_adj" ) {
    return EPOCH_FTHP_AUTO_ADJ;
  } else if ( pol == "guide" ) {
    return EPOCH_GUIDE;
  }
  cout << "bad epoch policy: " << pol << endl;
  return EPOCH_INVALID;
}

Epoch::Epoch(long length, long limit, EpochPolicy policy,
      double theta, MemoryBase& memory1, MemoryBase& memory2) :
      length(length), limit(limit), policy(policy), theta(theta),
      memory1(memory1), memory2(memory2) {

  epoch_ticks.name("epoch_ticks")
    .desc("epoch ticks")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  epoch_ticks = 0;

  epoch_ticks_phase
    .init(MAX_PHASES)
    .name("epoch_ticks_phase")
    .desc("epoch ticks phase")
    .precision(0)
    .memtype(MemType::BOTH)
    ;

  for (int i = 0; i < MAX_PHASES; i++) {
      epoch_ticks_phase[i] = 0;
  }

  pending_invalidate = false;
  pending_shootdown  = false;
  pending_preload    = false;
}

bool Epoch::try_invalidate() {
  if ( memory1.pending_requests() == 0 &&
       memory2.pending_requests() == 0) {
    pending_invalidate = false;
    memory1.invalidate_pages();
    memory2.invalidate_pages();
    return true;
  }
  return false;
}

void Epoch::tick(){

  if (need_epoch_invalidate()) {
    pending_invalidate = true;
  }

  if (need_epoch_shootdown()) {
    pending_shootdown = true;
  }

  if (need_epoch_preload()) {
    pending_preload = true;
  }

  epoch_ticks++;
  int cur_phase = Config::get_cur_phase();
  if (cur_phase != INVALID_PHASE) {
    ++epoch_ticks_phase[cur_phase];
  }
}

void Epoch::load_hot_pages(Core &core){
  sorted_pages.clear();

  for(auto it = memory1.page_counts_begin(); it != memory1.page_counts_end(); it++){
    sorted_pages.insert(pair<long, long>(it->second, it->first));
    //cout << "  tier1: " << it->first << " " << it->second << " "
    //     << memory1.has_page_status(it->first) << endl;
    if (core.get_page_memtype(it->first) == MemType::UNCLASSIFIED) {
      cout << "warning: unknown memtype for page on tier1: "
           << it->first << " " << it->second << endl;
      core.set_page_memtype(it->first, MemType::TIER1);
    }
  }
  for(auto it = memory2.page_counts_begin(); it != memory2.page_counts_end(); it++){
    sorted_pages.insert(pair<long, long>(it->second, it->first));
    //cout << "  tier2: " << it->first << " " << it->second << " "
    //     << memory2.has_page_status(it->first) << endl;
    if (core.get_page_memtype(it->first) == MemType::UNCLASSIFIED) {
      cout << "warning: unknown memtype for page on tier2: "
           << it->first << " " << it->second << endl;
      core.set_page_memtype(it->first, MemType::TIER2);
    }
  }

  //int sp_size = sorted_pages.size();
  //cout << "epoch_load: " << endl;
  memory1.reset_page_counts();
  memory2.reset_page_counts(); 
  core.cur_hot_pages.clear();

  if(policy == EPOCH_FTHP_SORT){
    while(!sorted_pages.empty() && (core.cur_hot_pages.size() < limit)){
      auto top = sorted_pages.begin();
      //cout << "  " << top->second << " " << top->first << endl;
      core.cur_hot_pages.insert(top->second);
      sorted_pages.erase(top);
    }
  }else if(policy == EPOCH_FTHP_AUTO_ADJ){
    auto top = sorted_pages.begin();
    while(!sorted_pages.empty() && (top->first >= theta)){
      //cout << "  " << top->second << " " << top->first << endl;
      core.cur_hot_pages.insert(top->second);
      sorted_pages.erase(top);
      top = sorted_pages.begin();
    }
    //cout << "  info: " << sp_size << " " << theta << " "
    //     << core.cur_hot_pages.size() << endl;
    theta *= (double) core.cur_hot_pages.size()/limit;
  }
  cout << endl;

  return;
}

