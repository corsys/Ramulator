#ifndef __PROCESSOR_H
#define __PROCESSOR_H

#include "Cache.h"
#include "Config.h"
#include "Memory.h"
#include "Request.h"
#include "Statistics.h"
#include "Epoch.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <ctype.h>
#include <functional>

#define PAGE_SIZE 4096
#define CACHE_LINE_SIZE 64
#define PAGE_SHIFT 12
#define MT_UNLIMITED -1
#define PAGE_ADDR(x) (x>>PAGE_SHIFT)

#define TIER1_BYTES_PER_SEC (11.9*1024*1024*1024)
#define CYCLES_PER_SEC 3200000000
#define TIER1_BYTES_PER_CYCLE (TIER1_BYTES_PER_SEC / CYCLES_PER_SEC)

#define likely(x)   __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

namespace ramulator 
{

  enum TraceCommand {
    TC_READ,
    TC_WRITE,
    TC_UNMAP,
    TC_PHASE_START,
    TC_PHASE_END,
    TC_INVALID_HOT_PAGE,
    TC_MIGRATE_WITH_FAULT,
    TC_MIGRATE_SANS_FAULT,
    NR_TRACE_COMMANDS
  };

  class Trace {
    public:
      Trace(const char* trace_fname);
      // trace file format 1:
      // [# of bubbles(non-mem instructions)] [read address(dec or hex)] <optional: write address(evicted cacheline)>
      bool get_unfiltered_request(long& bubble_cnt, long& req_addr,
          Request::Type& req_type, MemType& memtype, vector<string>& inj_trace,
          bool pending_unmap);
      bool get_filtered_request(long& bubble_cnt, long& req_addr,
          Request::Type& req_type, MemType& memtype);

      // trace file format 2:
      // [address(hex)] [R/W]
      bool get_dramtrace_request(long& req_addr, Request::Type& req_type, MemType& req_memtype);
      static bool is_special_code(long bubble_cnt);
    private:
      ScalarStat trace_request_count;
      ScalarStat trace_t1_request_count;
      ScalarStat trace_t2_request_count;
      std::ifstream file;
      std::string trace_name;
  };


  class Window {
    public:
      int ipc = 4;
      int depth = 128;

      Window() : ready_list(depth), addr_list(depth, -1) {}
      bool is_full();
      bool is_empty();
      void insert(bool ready, long addr);
      long retire();
      void set_ready(long addr, int mask);

    private:
      int load = 0;
      int head = 0;
      int tail = 0;
      std::vector<bool> ready_list;
      std::vector<long> addr_list;
  };

  class Core {
    public:
      long clk = 0;
      long retired = 0;
      int id = 0;
      function<bool(Request)> send1;
      function<bool(Request)> send2;

      Core(const Config& configs1, const Config& configs2, 
          int coreid, const char* trace_fname,
          function<bool(Request)> send_next1,
          function<bool(Request)> send_next2, 
          Cache* llc, std::shared_ptr<CacheSystem> cachesys, 
          MemoryBase& memory1, MemoryBase& memory2);

      void tick();
      void receive(Request& req);
      void invalidate_pages();
      bool migrate(long addr1, long addr2);
      double calc_ipc();
      bool finished();
      bool has_reached_limit();
      void set_pending_tlb(bool);
      bool pending_tlb();
      function<void(Request&)> callback;

      bool no_core_caches = true;
      bool no_shared_cache = true;
      int l1_size = 1 << 15;
      int l1_assoc = 1 << 3;
      int l1_blocksz = 1 << 6;
      int l1_mshr_num = 16;

      int l2_size = 1 << 18;
      int l2_assoc = 1 << 3;
      int l2_blocksz = 1 << 6;
      int l2_mshr_num = 16;
      std::vector<std::shared_ptr<Cache>> caches;
      Cache* llc;

      ScalarStat record_cycs;
      ScalarStat record_insts;
      long expected_limit_insts;
      // This is set true iff expected number of instructions has been executed or all instructions are executed.
      bool reached_limit = false;;
      std::set<long> cur_hot_pages;

      MemType get_page_memtype(long addr) {
        auto it = page_memtype.find(addr);
        if (it != page_memtype.end()) {
          return it->second;
        }
        return MemType::UNCLASSIFIED;
      }

      void set_page_memtype(long addr, MemType mt) {
        page_memtype[addr] = mt;
      }

    private:
      Trace trace;
      Window window;
      Epoch *epoch;

      long bubble_cnt;
      long req_addr = -1;
      Request::Type req_type;
      bool more_reqs;
      long last = 0;
      MemType memtype;
      MemType req_memtype;
      MemType get_actual_memtype(MemType req_mem);
      bool need_room_for_page(MemType mt);
      long make_room_for_page(MemType mt, bool need_fault);
      void migrate_to_tier(MemType dst, MemType src, vector<long> candidates, long& nr_migrated);
      std::map<long, MemType> page_memtype;
      bool _pending_tlb;

      ScalarStat cpu_inst;
      VectorStat cpu_inst_phase;
      ScalarStat page_fault_inst;
      VectorStat page_fault_inst_phase;

      ScalarStat memory_access_cycles1;
      VectorStat memory_access_cycles1_phase;
      MemoryBase& memory1;

      ScalarStat memory_access_cycles2;
      VectorStat memory_access_cycles2_phase;
      MemoryBase& memory2;

      ScalarStat preload_pages;
      ScalarStat t1_victims;
      ScalarStat t1_hot_victims;
      ScalarStat t1_migrate_with_fault;
      ScalarStat t2_migrate_with_fault;
      ScalarStat t1_migrate_sans_fault;
      ScalarStat t2_migrate_sans_fault;
      VectorStat preload_pages_phase;
      VectorStat t1_victims_phase;
      VectorStat t1_hot_victims_phase;
      VectorStat t1_migrate_with_fault_phase;
      VectorStat t2_migrate_with_fault_phase;
      VectorStat t1_migrate_sans_fault_phase;
      VectorStat t2_migrate_sans_fault_phase;

      ScalarStat tlb_shootdowns;
      VectorStat tlb_shootdowns_phase;
      ScalarStat tlb_shootdown_inst;
      VectorStat tlb_shootdown_inst_phase;

      ScalarStat phase_changes;

      ScalarStat migrate_cycles;
      ScalarStat migrate_cycles_less_fault;

      MemoryBase& get_memory(MemType mt) {
        return mt == MemType::TIER1 ? memory1 : memory2;
      }

      void inject_page_move_cmds(MemType dst, MemType src, long addr, bool need_fault);
      void inject_mem_cmd(long bubble_cnt, MemType mt, long addr, TraceCommand tc);
      void preload(set<long> hot_pages);
      void history_guide_preload(set<long> hot_page_set);
      void random_guide_preload(set<long> hot_page_set);
      void default_preload(set<long> hot_page_set);

      bool pending_unmap = false;
      vector<string> inj_trace;
  };

  class Processor {
    public:
      Processor(const Config& configs1, const Config& configs2, vector<const char*> trace_list,
          function<bool(Request)> send1,
          function<bool(Request)> send2, 
          MemoryBase& memory1, MemoryBase& memory2);
      void tick();
      void receive(Request& req);
      bool finished();
      bool has_reached_limit();

      static bool _phase_begin;
      static bool at_phase_begin()        { return _phase_begin; }
      static void set_phase_begin(bool b) { _phase_begin = b;    }

      static bool _pending_preload;
      static bool pending_preload()           { return _pending_preload; }
      static void set_pending_preload(bool b) { _pending_preload = b;    }

      std::vector<std::unique_ptr<Core>> cores;
      std::vector<double> ipcs;
      double ipc = 0;

      // When early_exit is true, the simulation exits when the earliest trace finishes.
      bool early_exit;

      bool no_core_caches = true;
      bool no_shared_cache = true;

      int l3_size = 1 << 23;
      int l3_assoc = 1 << 4;
      int l3_blocksz = 1 << 6;
      int mshr_per_bank = 16;

      std::shared_ptr<CacheSystem> cachesys;
      Cache llc;

      ScalarStat cpu_cycles;
      VectorStat cpu_cycles_phase;
  };

}
#endif /* __PROCESSOR_H */
