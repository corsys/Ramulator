#include "Processor.h"
#include <cassert>
#include <iomanip>
#include <sstream>
#include "Params.h"

using namespace std;
using namespace ramulator;

const char* long_memtype_str[] = {
  "tier 1",
  "tier 2",
  "unclassified"
};

const char* short_memtype_str[] = {
  "t1",
  "t2",
  "un"
};

bool Processor::_pending_preload = false;
bool Processor::_phase_begin = false;

Processor::Processor(const Config& configs1, const Config& configs2,
    vector<const char*> trace_list,
    function<bool(Request)> send_memory1,
    function<bool(Request)> send_memory2,
    MemoryBase& memory1, MemoryBase& memory2)
    : ipcs(trace_list.size(), -1),
    early_exit(configs1.is_early_exit()),
    no_core_caches(!configs1.has_core_caches()),
    no_shared_cache(!configs1.has_l3_cache()),
    cachesys(new CacheSystem(configs1, configs2, send_memory1, send_memory2)),
    llc(l3_size, l3_assoc, l3_blocksz,
         mshr_per_bank * trace_list.size(),
         Cache::Level::L3, cachesys) {

  assert(cachesys != nullptr);
  int tracenum = trace_list.size();
  assert(tracenum > 0);
  printf("tracenum: %d\n", tracenum);
  for (int i = 0 ; i < tracenum ; ++i) {
    printf("trace_list[%d]: %s\n", i, trace_list[i]);
  }

  if (configs1.has_small_cache()) {
    llc = Cache(1 << 19, 32, 64, mshr_per_bank * trace_list.size(),
                Cache::Level::L3, cachesys);
  }

  if (no_shared_cache) {
    for (int i = 0 ; i < tracenum ; ++i) {
      cores.emplace_back(new Core(
          configs1, configs2, i, trace_list[i], 
          send_memory1, send_memory2, nullptr,
          cachesys, memory1, memory2));
    }
  } else {
    for (int i = 0 ; i < tracenum ; ++i) {
      cores.emplace_back(new Core(configs1, configs2, i, trace_list[i],
          std::bind(&Cache::send, &llc, std::placeholders::_1), send_memory2,
          &llc, cachesys, memory1, memory2));
    }
  }
  for (int i = 0 ; i < tracenum ; ++i) {
    cores[i]->callback = std::bind(&Processor::receive, this,
        placeholders::_1);
  }

  // regStats
  cpu_cycles.name("cpu_cycles")
            .desc("cpu cycle number")
            .memtype(MemType::BOTH)
            .precision(0)
            ;
  cpu_cycles = 0;

  cpu_cycles_phase
            .init(MAX_PHASES)
            .name("cpu_cycles_phase")
            .desc("CPU cycle of the whole execution")
            .precision(0)
            .memtype(MemType::BOTH)
            ;
  for (int i = 0; i < MAX_PHASES; i++) {
      cpu_cycles_phase[i] = 0;
  }
}

void Processor::tick() {
  cpu_cycles++;
  int cur_phase = Config::get_cur_phase();
  if (cur_phase != INVALID_PHASE) {
      cpu_cycles_phase[cur_phase]++;
  }

  if (!(no_core_caches && no_shared_cache)) {
    cachesys->tick();
  }
  for (unsigned int i = 0 ; i < cores.size() ; ++i) {
    Core* core = cores[i].get();
    core->tick();
  }
}

void Processor::receive(Request& req) {
  if (!no_shared_cache) {
    llc.callback(req);
  } else if (!cores[0]->no_core_caches) {
    // Assume all cores have caches or don't have caches
    // at the same time.
    for (unsigned int i = 0 ; i < cores.size() ; ++i) {
      Core* core = cores[i].get();
      core->caches[0]->callback(req);
    }
  }
  for (unsigned int i = 0 ; i < cores.size() ; ++i) {
    Core* core = cores[i].get();
    core->receive(req);
  }
}

bool Processor::finished() {
  if (early_exit) {
    for (unsigned int i = 0 ; i < cores.size(); ++i) {
      if (cores[i]->finished()) {
        for (unsigned int j = 0 ; j < cores.size() ; ++j) {
          ipc += cores[j]->calc_ipc();
        }
        return true;
      }
    }
    return false;
  } else {
    for (unsigned int i = 0 ; i < cores.size(); ++i) {
      if (!cores[i]->finished()) {
        return false;
      }
      if (ipcs[i] < 0) {
        ipcs[i] = cores[i]->calc_ipc();
        ipc += ipcs[i];
      }
    }
    return true;
  }
}

bool Processor::has_reached_limit() {
  for (unsigned int i = 0 ; i < cores.size() ; ++i) {
    if (!cores[i]->has_reached_limit()) {
      return false;
    }
  }
  return true;
}

Core::Core(const Config& configs1, const Config& configs2, int coreid,
    const char* trace_fname, function<bool(Request)> send_next1,
    function<bool(Request)> send_next2, Cache* llc,
    std::shared_ptr<CacheSystem> cachesys, MemoryBase& memory1,
    MemoryBase& memory2)
    : id(coreid), no_core_caches(!configs1.has_core_caches()),
    no_shared_cache(!configs1.has_l3_cache()),
    llc(llc), trace(trace_fname), memory1(memory1), memory2(memory2)
{
  // Build cache hierarchy
  if (no_core_caches) {
    send1 = send_next1;
    send2 = send_next2;
  } else {
#if 0
    // L2 caches[0]
    caches.emplace_back(new Cache(
        l2_size, l2_assoc, l2_blocksz, l2_mshr_num,
        Cache::Level::L2, cachesys));
    // L1 caches[1]
#endif
    // L1 caches[0]
    caches.emplace_back(new Cache(
        l1_size, l1_assoc, l1_blocksz, l1_mshr_num,
        Cache::Level::L1, cachesys));
    send1 = bind(&Cache::send, caches[0].get(), placeholders::_1);
    send2 = bind(&Cache::send, caches[0].get(), placeholders::_1);
    if (llc != nullptr) {
      caches[0]->concatlower(llc);
    }
    //caches[1]->concatlower(caches[0].get());
  }
  if (no_core_caches) {
    more_reqs = trace.get_unfiltered_request(
        bubble_cnt, req_addr, req_type, memtype, inj_trace, pending_unmap);
    if (memtype == MemType::TIER1) {
      req_addr = memory1.page_allocator(req_addr, id);
    } else if (memtype == MemType::TIER2) {
      req_addr = memory2.page_allocator(req_addr, id);
    }
  } else {
    more_reqs = trace.get_unfiltered_request(
        bubble_cnt, req_addr, req_type, memtype, inj_trace, pending_unmap);
    if (memtype == MemType::TIER1) {
      req_addr = memory1.page_allocator(req_addr, id);
    } else if (memtype == MemType::TIER2) {
      req_addr = memory2.page_allocator(req_addr, id);
    }
  }

  // set expected limit instruction for calculating weighted speedup
  expected_limit_insts = configs1.get_expected_limit_insts();
  
  Params *myp = Params::instance();
  if (myp->epoch_length != 0) {
    EpochPolicy policy = Epoch::get_epoch_policy(myp->epoch_policy);
    epoch = new Epoch(myp->epoch_length, myp->t1_limit, policy, 
                      ((double)myp->initial_theta), memory1, memory2);
  }

  // regStats
  record_cycs.name("record_cycs_core_" + to_string(id))
    .desc("Record cycle number for calculating weighted speedup. (Only valid when expected limit instruction number is non zero in config file.)")
    .precision(0)
    .memtype(MemType::BOTH)
    ;

  record_insts.name("record_insts_core_" + to_string(id))
    .desc("Retired instruction number when record cycle number. (Only valid when expected limit instruction number is non zero in config file.)")
    .precision(0)
    ;
  memory_access_cycles1.name("memory_access_cycles1_core_" + to_string(id))
    .desc("memory access cycles in memory time domain")
    .precision(0)
    .memtype(configs1.get_mem_type())
    ;
  memory_access_cycles1 = 0;

  memory_access_cycles2.name("memory_access_cycles2_core_" + to_string(id))
    .desc("memory access cycles in memory time domain")
    .precision(0)
    .memtype(configs2.get_mem_type())
    ;
  memory_access_cycles2 = 0;

  cpu_inst.name("cpu_instructions_core_" + to_string(id))
    .desc("cpu instruction number")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  cpu_inst = 0;

  page_fault_inst.name("page_fault_instructions_core_" + to_string(id))
    .desc("page fault instruction number")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  page_fault_inst = 0;

  tlb_shootdowns.name("tlb_shootdowns")
    .desc("tlb shootdowns")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  tlb_shootdowns = 0;

  tlb_shootdown_inst.name("tlb_shootdown_inst")
    .desc("tlb shootdown inst")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  tlb_shootdown_inst = 0;

  phase_changes.name("phase_changes_core_" + to_string(id))
    .desc("phase changes")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  phase_changes = 0;

  preload_pages.name("preload_pages_core_" + to_string(id))
    .desc("preload pages")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  preload_pages = 0;

  t1_victims.name("t1_victims_core_" + to_string(id))
    .desc("t1 victims")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  t1_victims = 0;

  t1_hot_victims.name("t1_hot_victims_core_" + to_string(id))
    .desc("t1 hot victims")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  t1_hot_victims = 0;

  t1_migrate_with_fault.name("t1_migrate_with_fault_core_" + to_string(id))
    .desc("t1 migrate with fault")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  t1_migrate_with_fault = 0;

  t2_migrate_with_fault.name("t2_migrate_with_fault_core_" + to_string(id))
    .desc("t2 migrate with fault")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  t2_migrate_with_fault = 0;

  t1_migrate_sans_fault.name("t1_migrate_sans_fault_core_" + to_string(id))
    .desc("t1 migrate sans fault")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  t1_migrate_sans_fault = 0;

  t2_migrate_sans_fault.name("t2_migrate_sans_fault_core_" + to_string(id))
    .desc("t2 migrate sans fault")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  t2_migrate_sans_fault = 0;

  migrate_cycles.name("migrate_cycles")
    .desc("migrate cycles")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  migrate_cycles = 0;

  migrate_cycles_less_fault.name("migrate_cycles_less_fault")
    .desc("migrate cycles")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  migrate_cycles_less_fault = 0;

  // per-phase stats
  int i;
  memory_access_cycles1_phase
                      .init(MAX_PHASES)
                      .name("memory_access_cycles1_phase_" + to_string(id))
                      .desc("cycle number in memory clock domain that there is at least one request in the queue of memory controller")
                      .precision(0)
                      .memtype(configs1.get_mem_type())
                      ;
  for (i = 0; i < MAX_PHASES; i++) {
      memory_access_cycles1_phase[i] = 0;
  }

  memory_access_cycles2_phase
                      .init(MAX_PHASES)
                      .name("memory_access_cycles2_phase_" + to_string(id))
                      .desc("cycle number in memory clock domain that there is at least one request in the queue of memory controller")
                      .precision(0)
                      .memtype(configs2.get_mem_type())
                      ;
  for (i = 0; i < MAX_PHASES; i++) {
      memory_access_cycles2_phase[i] = 0;
  }

  cpu_inst_phase
          .init(MAX_PHASES)
          .name("cpu_instructions_phase")
          .desc("commited cpu instruction number")
          .precision(0)
          .memtype(MemType::BOTH)
          ;

  for (i = 0; i < MAX_PHASES; i++) {
      cpu_inst_phase[i] = 0;
  }

  page_fault_inst_phase
          .init(MAX_PHASES)
          .name("page_fault_instructions_phase")
          .desc("page fault instruction number")
          .precision(0)
          .memtype(MemType::BOTH)
          ;

  for (i = 0; i < MAX_PHASES; i++) {
      page_fault_inst_phase[i] = 0;
  }

  tlb_shootdowns_phase
    .init(MAX_PHASES)
    .name("tlb_shootdowns_phase")
    .desc("tlb shootdowns phase")
    .precision(0)
    .memtype(MemType::BOTH)
    ;

  for (int i = 0; i < MAX_PHASES; i++) {
      tlb_shootdowns_phase[i] = 0;
  }

  tlb_shootdown_inst_phase
          .init(MAX_PHASES)
          .name("tlb_shootdown_instructions_phase")
          .desc("tlb shootdown instruction number")
          .precision(0)
          .memtype(MemType::BOTH)
          ;

  for (i = 0; i < MAX_PHASES; i++) {
      tlb_shootdown_inst_phase[i] = 0;
  }

  preload_pages_phase
    .init(MAX_PHASES)
    .name("preload_pages_phase")
    .desc("preload pages phase")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  for (i = 0; i < MAX_PHASES; i++) {
      preload_pages_phase[i] = 0;
  }

  t1_victims_phase
    .init(MAX_PHASES)
    .name("t1_victims_phase")
    .desc("t1 victims phase")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  for (i = 0; i < MAX_PHASES; i++) {
      t1_victims_phase[i] = 0;
  }

  t1_hot_victims_phase
    .init(MAX_PHASES)
    .name("t1_hot_victims_phase")
    .desc("t1 hot victims phase")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  for (i = 0; i < MAX_PHASES; i++) {
      t1_hot_victims_phase[i] = 0;
  }

  t1_migrate_with_fault_phase
    .init(MAX_PHASES)
    .name("t1_migrate_with_fault_phase")
    .desc("t1 migrate with fault phase")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  for (i = 0; i < MAX_PHASES; i++) {
      t1_migrate_with_fault_phase[i] = 0;
  }

  t2_migrate_with_fault_phase
    .init(MAX_PHASES)
    .name("t2_migrate_with_fault_phase")
    .desc("t2 migrate with fault phase")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  for (i = 0; i < MAX_PHASES; i++) {
      t2_migrate_with_fault_phase[i] = 0;
  }

  t1_migrate_sans_fault_phase
    .init(MAX_PHASES)
    .name("t1_migrate_sans_fault_phase")
    .desc("t1 migrate sans phase")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  for (i = 0; i < MAX_PHASES; i++) {
      t1_migrate_sans_fault_phase[i] = 0;
  }

  t2_migrate_sans_fault_phase
    .init(MAX_PHASES)
    .name("t2_migrate_sans_fault_phase")
    .desc("t2 migrate sans fault phase")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  for (i = 0; i < MAX_PHASES; i++) {
      t2_migrate_sans_fault_phase[i] = 0;
  }
}


double Core::calc_ipc()
{
  printf("[%d]retired: %ld, clk, %ld\n", id, retired, clk);
  return (double) retired / clk;
}

MemType Core::get_actual_memtype(MemType req_mem) {
  if (req_mem == MemType::TIER1 && !(get_memory(MemType::TIER1).has_space())) {
    return MemType::TIER2;
  }
  return req_memtype;
}


void Core::invalidate_pages(){
  memory1.invalidate_pages();
  memory2.invalidate_pages();
}

void Core::inject_mem_cmd(long bubble_cnt, MemType mt, long addr,
  TraceCommand tc) {

  char buf[60];
  sprintf(buf, "%ld %d %ld %d", bubble_cnt, mt, addr, tc);
  string cmd = buf;
  inj_trace.push_back(cmd);
}

void Core::inject_page_move_cmds(MemType dst, MemType src, long addr,
  bool need_fault) {
  int i;

  /* We currently only do the read. We may add the write later if we feel if
   * it is necessary.
   */
  if (Params::instance()->migrate_penalty) {
      long cur_addr = addr;
      for (i = 0; i < (PAGE_SIZE / CACHE_LINE_SIZE); i++) {
        inject_mem_cmd(0, src, cur_addr, TC_READ);
        cur_addr += CACHE_LINE_SIZE;
      }
  }

  /* the processor and memory will update their bookkeeping structures after
   * processing this migrate command
   */
  if (need_fault) {
    inject_mem_cmd(0, dst, (addr>>12), TC_MIGRATE_WITH_FAULT);
  } else {
    inject_mem_cmd(0, dst, (addr>>12), TC_MIGRATE_SANS_FAULT);
  }


  /* set the moving page's status to ZOMBIE to indicate that it should not
   * cause a fault when we go to move the page
   */
  long page_addr = (addr >> 12);
  auto pgtype_it = page_memtype.find(page_addr);
  assert(pgtype_it != page_memtype.end());

  MemoryBase& mem = get_memory(page_memtype[page_addr]);
  mem.set_page_status(page_addr, MemStatus::ZOMBIE);

  /* take care of the migrate stats */
  int cur_phase = Config::get_cur_phase();
  if (need_fault) {
    if (dst == MemType::TIER1) {
      ++t1_migrate_with_fault;
      if (cur_phase != INVALID_PHASE) {
        ++t1_migrate_with_fault_phase[cur_phase];
      }
    } else {
      ++t2_migrate_with_fault;
      if (cur_phase != INVALID_PHASE) {
        ++t2_migrate_with_fault_phase[cur_phase];
      }
    }
  } else {
    if (dst == MemType::TIER1) {
      ++t1_migrate_sans_fault;
      if (cur_phase != INVALID_PHASE) {
        ++t1_migrate_sans_fault_phase[cur_phase];
      }
    } else {
      ++t2_migrate_sans_fault;
      if (cur_phase != INVALID_PHASE) {
        ++t2_migrate_sans_fault_phase[cur_phase];
      }
    }
  }
}

bool Core::need_room_for_page(MemType mt) {
  return (mt == MemType::TIER1 &&
          !(get_memory(MemType::TIER1).has_empty_space()));
}

long Core::make_room_for_page(MemType mt, bool need_fault) {

  int cur_phase = Config::get_cur_phase();
  assert (mt == MemType::TIER1);
  MemoryBase& mem = get_memory(mt);

  long victim = mem.get_victim();
  assert(victim != 0x0);
  
  ++t1_victims;
  if (mem.page_status(victim) == MemStatus::INVALID_HOT) {
    ++t1_hot_victims;
  }
  //cout << "vic: " << victim << " " << mem.page_status(victim);

  if (cur_phase != INVALID_PHASE) {
    ++t1_victims_phase[cur_phase];
    if (mem.page_status(victim) == MemStatus::INVALID_HOT) {
      ++t1_hot_victims_phase[cur_phase];
    }
  }

  inject_page_move_cmds(MemType::TIER2, MemType::TIER1, (victim<<12), need_fault);
  return victim;
}

void Core::migrate_to_tier(MemType dst_tier, MemType src_tier,
  vector<long> candidates, long& nr_migrated) {

  unsigned int dst_free, dst_empty;

  MemoryBase& dst_mem = get_memory(dst_tier);

  dst_free  = dst_mem.nr_free_pages();
  dst_empty = dst_mem.nr_empty_pages();

#if 0
  cout << "migrate_to_tier: " << dst_tier << " " << src_tier << " "
       << candidates.size() << " " << dst_free << " " << dst_empty << endl;
#endif

#if 0
  //cout << "pages in tier1: " << endl;
  for (auto page_it = page_memtype.begin();
       page_it != page_memtype.end();
       ++page_it) {
    if (page_it->second == MemType::TIER1) {
      //cout << "  " << page_it->first << " " << (page_it->first<<12) << " "
      //     << tier1.page_status(page_it->first) << endl;
    }
  }
#endif

  while (!candidates.empty() && dst_free > 0) {

    long promo_page = candidates[rand() % candidates.size()];
#if 0
    cout << "  mig: " << promo_page << " " << (promo_page<<12)
         << " ";
#endif
    
    if (dst_empty != 0) {
      dst_empty--;
      dst_mem.mark_empty_slot(promo_page);
      //cout << "empty";
    } else {
      //cout << "room: " << promo_page << " " << (promo_page<<12) << " ";
      make_room_for_page(dst_tier, false);
    }
    //cout << endl;
    dst_free--;

    inject_page_move_cmds(dst_tier, src_tier,
                          (promo_page<<12), false);

    candidates.erase( remove(candidates.begin(), candidates.end(),
                      promo_page), candidates.end() );

    nr_migrated++;
    preload_pages++;
    if(Config::in_valid_phase()){
      int cur_phase = Config::get_cur_phase();
      preload_pages_phase[cur_phase]++;
    }
  }
}

void Core::history_guide_preload(set<long> hot_page_set) {
  vector<long> candidates;
  vector<long> hot_pages(hot_page_set.begin(), hot_page_set.end());
  long nr_migrated, bytes_moved, interval_cycles;

  nr_migrated = 0;

  MemoryBase& tier1 = get_memory(MemType::TIER1);
  MemoryBase& tier2 = get_memory(MemType::TIER2);

  tier2.validate_out_pages(hot_pages);

  /* validate the hot, faulted pages in tier1 and migrate any that are in
   * tier2 into tier1.
   */
  tier1.get_invalid_faulted_pages_in_set(hot_pages, candidates);
  tier1.validate_in_pages(candidates);

  candidates.clear();
  tier2.get_invalid_faulted_pages_in_set(hot_pages, candidates);
  migrate_to_tier(MemType::TIER1, MemType::TIER2, candidates, nr_migrated);

  /* now do the accessed hot pages */
  candidates.clear();
  tier1.get_invalid_accessed_pages_in_set(hot_pages, candidates);
  tier1.validate_in_pages(candidates);

  candidates.clear();
  tier2.get_invalid_accessed_pages_in_set(hot_pages, candidates);
  migrate_to_tier(MemType::TIER1, MemType::TIER2, candidates, nr_migrated);

  /* if there's still space -- do the unaccessed, unfaulted hot pages */
  candidates.clear();
  tier1.get_invalid_pages_in_set(hot_pages, candidates);
  tier1.validate_in_pages(candidates);

  candidates.clear();
  tier2.get_invalid_pages_in_set(hot_pages, candidates);
  migrate_to_tier(MemType::TIER1, MemType::TIER2, candidates, nr_migrated);

  /* validate pages left in tier 2 that have not been promoted */
  tier2.validate_in_pages(hot_pages);

  /* get remaining invalid pages in tier1 and migrate to tier2 */
  candidates.clear();
  tier1.get_invalid_pages_out_set(hot_pages, candidates);
  migrate_to_tier(MemType::TIER2, MemType::TIER1, candidates, nr_migrated);

  bytes_moved      = (nr_migrated * PAGE_SIZE);
  interval_cycles  = (bytes_moved / TIER1_BYTES_PER_CYCLE);
  migrate_cycles  += interval_cycles;
  migrate_cycles_less_fault += (interval_cycles - PAGE_FAULT_PENALTY);
}

void Core::random_guide_preload(set<long> hot_page_set) {
  vector<long> tier1_candidates;
  vector<long> tier2_candidates;
  vector<long> all_candidates;
  vector<long> rand_candidates;
  vector<long> hot_pages(hot_page_set.begin(), hot_page_set.end());
  long nr_migrated, bytes_moved, interval_cycles;

  nr_migrated = 0;

  MemoryBase& tier1 = get_memory(MemType::TIER1);
  MemoryBase& tier2 = get_memory(MemType::TIER2);

  tier1.get_invalid_pages_in_set(hot_pages, tier1_candidates);
  tier2.get_invalid_pages_in_set(hot_pages, tier2_candidates);

  for (auto it = tier1_candidates.begin(); it != tier1_candidates.end(); ++it) {
    all_candidates.push_back(*it);
  }
  for (auto it = tier2_candidates.begin(); it != tier2_candidates.end(); ++it) {
    all_candidates.push_back(*it);
  }

  unsigned int i = 0;
  unsigned int t1_free = tier1.nr_free_pages();
  while (i < t1_free && !all_candidates.empty()) {
    long promo_page = all_candidates[rand() % all_candidates.size()];
    rand_candidates.push_back(promo_page);
    all_candidates.erase( remove(all_candidates.begin(), all_candidates.end(),
                                 promo_page), all_candidates.end() );
    i++;
  }

  tier1.validate_in_pages(rand_candidates);
  tier2.validate_out_pages(rand_candidates);

  /* handle selected pages on tier2 */
  tier2_candidates.clear();
  tier2.get_invalid_pages_in_set(rand_candidates, tier2_candidates);
  migrate_to_tier(MemType::TIER1, MemType::TIER2, tier2_candidates, nr_migrated);

  /* validate the candidates left on tier2 */
  tier2.validate_in_pages(tier2_candidates);

  /* get remaining invalid pages in tier1 and migrate to tier2 */
  tier1_candidates.clear();
  tier1.get_invalid_pages_out_set(hot_pages, tier1_candidates);
  migrate_to_tier(MemType::TIER2, MemType::TIER1, tier1_candidates, nr_migrated);

  bytes_moved      = (nr_migrated * PAGE_SIZE);
  interval_cycles  = (bytes_moved / TIER1_BYTES_PER_CYCLE);
  migrate_cycles  += interval_cycles;
  migrate_cycles_less_fault += (interval_cycles - PAGE_FAULT_PENALTY);
}

/* assumes all pages on tier1 and tier2 are INVALID or INVALID_HOT on entry */
void Core::default_preload(set<long> hot_page_set) {

  vector<long> candidates;
  vector<long> hot_pages(hot_page_set.begin(), hot_page_set.end());
  long nr_migrated, bytes_moved, interval_cycles;

  MemoryBase& tier1 = get_memory(MemType::TIER1);
  MemoryBase& tier2 = get_memory(MemType::TIER2);

  nr_migrated = 0;

  //cout << "preload_start " << endl;

  /* Validate all of the pages that are already in the correct tier */
  tier1.validate_in_pages(hot_pages);
  tier2.validate_out_pages(hot_pages);

  /* hot pages on tier2 comprise our candidate set */
  tier2.get_invalid_pages_in_set(hot_pages, candidates);
  migrate_to_tier(MemType::TIER1, MemType::TIER2, candidates, nr_migrated);

  /* validate pages left in the candidate set in tier 2 */
  tier2.validate_in_pages(candidates);

  /* get remaining invalid pages in tier1 and migrate to tier2 */
  candidates.clear();
  tier1.get_invalid_pages_out_set(hot_pages, candidates);
  migrate_to_tier(MemType::TIER2, MemType::TIER1, candidates, nr_migrated);

  bytes_moved      = (nr_migrated * PAGE_SIZE);
  interval_cycles  = (bytes_moved / TIER1_BYTES_PER_CYCLE);
  migrate_cycles  += interval_cycles;
  migrate_cycles_less_fault += (interval_cycles - PAGE_FAULT_PENALTY);
}

/* assumes all pages on tier1 and tier2 are INVALID or INVALID_HOT on entry */
void Core::preload(set<long> hot_pages) {

  MemoryBase& tier1 = get_memory(MemType::TIER1);
  MemoryBase& tier2 = get_memory(MemType::TIER2);

  if (Params::instance()->history_guide) {
    history_guide_preload(hot_pages);
  } else if (Params::instance()->random_guide) {
    random_guide_preload(hot_pages);
  } else {
    default_preload(hot_pages);
  }

  tier1.reset_page_migrate_status();
  tier2.reset_page_migrate_status();

  tier1.reset_page_faults();
  tier2.reset_page_faults();
  tier1.reset_page_counts();
  tier2.reset_page_counts();

  /* pay the page fault penalty */
  tier1.set_pending_fault(true);
}

#if 0
/* assumes all pages on tier1 and tier2 are INVALID or INVALID_HOT on entry */
void Core::preload(set<long> hot_pages) {

  vector<long> candidates;
  set<long> cur_hot_pages;
  int t1_free, t1_empty;

  //cout << "preload_start " << endl;
  MemoryBase& tier1 = get_memory(MemType::TIER1);
  MemoryBase& tier2 = get_memory(MemType::TIER2);

  /* Validate all of the pages that are already in the correct tier */
  tier1.validate_in_pages(hot_pages);
  tier2.validate_out_pages(hot_pages);

  /* Next, build a candidate set of all the pages that should move from tier 2
   * to tier 1, if there is room
   */
  for (auto page_it = hot_pages.begin();
       page_it != hot_pages.end(); page_it++) {

    auto mtit = page_memtype.find(*page_it);
    if (mtit == page_memtype.end()) {
      cout << "no memtype! " << *page_it << endl;
      assert (false && "page with no memtype!");
    }

    /* hot pages that are on tier 2 comprise the candidate set */
    if (mtit->second == MemType::TIER2) {
      if (tier2.page_status(*page_it) != MemStatus::INVALID) {
        cout << "addr: " << (*page_it) << " " << ((*page_it)<<12) << " "
             << tier2.page_status(*page_it) << endl;
        assert(false && "valid hot page on tier 2");
        //cout << "addr: " << (*page_it) << " " << ((*page_it)<<12) << " "
        //     << tier2.page_status(*page_it) << endl;
      }
      candidates.push_back(*page_it);
    }
  }

  /* while there are still candidates and tier1 has space -- move pages to
   * tier1
   */
  t1_free  = tier1.nr_free_pages();
  t1_empty = tier1.nr_empty_pages();
  cout << "preload: " << candidates.size() << " " << t1_free << endl;
#if 0
  //cout << "pages in tier1: " << endl;
  for (auto page_it = page_memtype.begin();
       page_it != page_memtype.end();
       ++page_it) {
    if (page_it->second == MemType::TIER1) {
      //cout << "  " << page_it->first << " " << (page_it->first<<12) << " "
      //     << tier1.page_status(page_it->first) << endl;
    }
  }
#endif

  while (!candidates.empty() && t1_free > 0) {

    long promo_page = candidates[rand() % candidates.size()];
    cout << "promo: " << promo_page << " " << (promo_page<<12)
         << " " << endl;
    
    if (t1_empty != 0) {
      t1_empty--;
    } else {
      //cout << "room: " << promo_page << " " << (promo_page<<12) << " ";
      make_room_for_page(MemType::TIER1, false);
    }
    t1_free--;

    inject_page_move_cmds(MemType::TIER1, MemType::TIER2,
                          (promo_page<<12), false);

    candidates.erase( remove(candidates.begin(), candidates.end(),
                             promo_page), candidates.end() );

    preload_pages++;
    if(Config::in_valid_phase()){
      int cur_phase = Config::get_cur_phase();
      preload_pages_phase[cur_phase]++;
    }
  }

  /* validate pages left in the candidate set in tier 2 */
  set<long> cand_set(candidates.begin(), candidates.end());
  tier2.validate_in_pages(cand_set);

  /* MRJ: for the phase-based guidance configs -- if we _know_ any pages are
   * going to be cold after the preload, but they are still on tier 1, we
   * should migrate them to tier2 and leave the space empty for tier1.
   *
   * By leaving them invalid, we're incurring faults we dont need to incur. As
   * it is now, they will fault and migrate to tier2 on their first touch.
   *
   * Can we do this with the epoch configs?
   *
   * With meswani's approach, these pages will need to fault anyway (won't at
   * all with the history-based sorting approach).
   *
   * With the guidance-based configs with epochs, the set of hot pages might
   * change again after the preload, but before the next epoch expires. Pages
   * that we validate on tier2 might become hot again during the epoch -- and if
   * there is free space on tier1 -- we end up wasting it. I think we should
   * only do this with the phase-based guidance configs.
   *
   * However, I don't expect this to change the results much at all -- it just
   * seems that these sorts of faults occur so infrequently that they are not
   * likely to affect performance.
   */
#if 0
  /* might be a bit complicated to write -- do this later */
  if (Params::instance()->phase_change_free) {
    // find pages that are not hot whose memtype is tier1
    // but -- can't use the candidate set (we've already removed some pages
    // that were hot from the candidate set)
    // and we can't consider everything that is currently on tier1 -- some of
    // those pages are in the process of being migrated out
    
  }
#endif

  /* pay the page fault penalty */
  tier1.set_pending_fault(true);

  return;
}
#endif

void Core::tick()
{
  clk++;
#if 0
  if ( (clk % 1000000) == 0 ) {
    cout << clk << endl;
  }
#endif

  if (epoch) {

    if (Processor::at_phase_begin()) {
      if ( (clk % epoch->length) == 0 ) {
        epoch->set_pending_tick(true);
      }
    } else {
      if ( (clk % epoch->length) == 0 || epoch->has_pending_tick()) {
        epoch->tick();
        epoch->set_pending_tick(false);
      }

      if ( epoch->is_pending_shootdown() ) {

        if (Params::instance()->shootdown_penalty) {
          bubble_cnt += TLB_SHOOTDOWN_PENALTY;
          tlb_shootdowns++;
          tlb_shootdown_inst += TLB_SHOOTDOWN_PENALTY;
          int cur_phase = Config::get_cur_phase();
          if (cur_phase != INVALID_PHASE) {
            ++tlb_shootdowns_phase[cur_phase];
            tlb_shootdown_inst_phase[cur_phase] += TLB_SHOOTDOWN_PENALTY;
          }
        }

        epoch->clear_pending_shootdown();
      }

      if (epoch->is_pending_invalidate()) {
        if (!epoch->try_invalidate()) {
          return;
        }
      }

      /* by this point, all pages are invalidated */
      if (epoch->is_pending_preload()) {
        if (epoch->policy != EPOCH_GUIDE) {
          epoch->load_hot_pages(*this);
        }
        preload(cur_hot_pages);
        epoch->clear_pending_preload();
      }
    }
  }

  if (Processor::pending_preload()) {
    preload(cur_hot_pages);
    Processor::set_pending_preload(false);
  }

  retired += window.retire();

  if (expected_limit_insts == 0 && !more_reqs) return;

  // bubbles (non-memory operations)
  int inserted = 0;
  while (bubble_cnt > 0) {
    if (inserted == window.ipc) return;
    if (window.is_full()) return;

    window.insert(true, -1);
    inserted++;
    bubble_cnt--;
    cpu_inst++;
    int cur_phase = Config::get_cur_phase();
    if (cur_phase != INVALID_PHASE) {
      cpu_inst_phase[cur_phase]++;
    }
    if (long(cpu_inst.value()) == expected_limit_insts && !reached_limit) {
      record_cycs = clk;
      record_insts = long(cpu_inst.value());
      memory1.record_core(id);
      memory2.record_core(id);
      reached_limit = true;
    }
  }

  // need to get the right memtype for reads and writes
  if (req_type == Request::Type::READ || req_type == Request::Type::WRITE) {
    long page_addr = (req_addr >> 12);
    auto pgtype_it = page_memtype.find(page_addr);

    if (pgtype_it != page_memtype.end()) {

      MemType cur_memtype = pgtype_it->second;
      MemoryBase& mem = get_memory(cur_memtype);

#if 0
      if (Config::get_cur_phase() == 0) {
        cout << "(C): " << req_addr << " " << page_addr << " "
             << req_memtype << " " << cur_memtype << " "
             << mem.page_status(page_addr) << endl;
      }
#endif

      if ( (mem.page_status(page_addr) == MemStatus::INVALID) ||
           (mem.page_status(page_addr) == MemStatus::INVALID_HOT) ) {

        /* If the page exists, but is invalid, we have two cases:
         *
         * 1) If the cur_memtype and actual_memtype do not match, we need to
         * migrate the page to the appropriate tier -- possibly evicting an
         * invalid page from TIER1, if necessary.
         *
         * 2) If the cur_memtype and actual_memtype match, we simply set the
         * page's status to reserved and let it fault on the next access.
         */

        MemType actual_memtype = get_actual_memtype(req_memtype);
        if (actual_memtype != cur_memtype) {

          //cout << "(D): " << req_addr << " " << page_addr << " "
          //     << req_memtype << " " << actual_memtype << endl;
          if (need_room_for_page(actual_memtype)) {
            //cout << "room: " << page_addr << " " << req_addr << " ";
            make_room_for_page(actual_memtype, true);
          }

          inject_page_move_cmds(actual_memtype, cur_memtype, (page_addr<<12), true);
          
          /* Skip the command that caused the migration and inject it into the
           * instruction stream so that we can pick it up again after we're
           * done handling the swap.
           */
          inject_mem_cmd(0, actual_memtype, req_addr,
                         ((req_type == Request::Type::READ) ?
                          TC_READ : TC_WRITE) );
          req_type = Request::Type::SKIP;

        } else {

          //cout << "(C): " << req_addr << " " << page_addr << " "
          //     << req_memtype << " " << actual_memtype << endl;
          mem.set_page_status(page_addr, MemStatus::RESERVED);

        }
      }

    } else {
      MemType actual_memtype = get_actual_memtype(req_memtype);
#if 0
      if (Config::get_cur_phase() == 0) {
        cout << "(D): " << req_addr << " " << page_addr << " "
             << req_memtype << " " << actual_memtype << endl;
      }
#endif
      if (need_room_for_page(actual_memtype)) {

        //cout << "(A): " << req_addr << " " << page_addr << " "
        //     << req_memtype << " " << actual_memtype << endl;
        //cout << "room: " << page_addr << " " << req_addr << " ";
        make_room_for_page(actual_memtype, true);
        inject_mem_cmd(0, actual_memtype, req_addr,
                       ((req_type == Request::Type::READ) ?
                        TC_READ : TC_WRITE) );
        req_type = Request::Type::SKIP;

      } else {

        //cout << "(B): " << req_addr << " " << page_addr << " "
        //     << req_memtype << " " << actual_memtype << endl;
        //cout << "set_memtype: " << page_addr << " "
        //     << actual_memtype << endl;
        page_memtype[page_addr] = actual_memtype;
        MemoryBase& mem = get_memory(page_memtype[page_addr]);
        mem.set_page_status(page_addr, MemStatus::RESERVED);

      }
    }

    // memtype is correct now
    if (req_type != Request::Type::SKIP) {
      memtype = page_memtype[page_addr];
    }
  }


  if (req_type == Request::Type::READ) {
    // read request

    if (inserted == window.ipc) return;
    if (window.is_full()) return;

    Request req(req_addr, req_type, memtype, req_memtype, callback, id);
    if (memtype == MemType::TIER1 && !send1(req)) {
      return;
    } else if (memtype == MemType::TIER2 && !send2(req)) {
      return;
    } else if (memtype != MemType::TIER1 && memtype != MemType::TIER2) {
      cerr << "Erroneous memtype: " << (int)memtype << endl;
      assert(false && "Invalid MemType");
    }

    if (epoch && epoch->policy == EPOCH_GUIDE) {
      if (req_memtype == MemType::TIER1) {
        cur_hot_pages.insert((req_addr >> 12));
      }
    }

    window.insert(false, req_addr);
  }
  else if (req_type == Request::Type::WRITE){
    // write request

    Request req(req_addr, req_type, memtype, req_memtype, callback, id);
    if (memtype == MemType::TIER1 && !send1(req)) {
      return;
    } else if (memtype == MemType::TIER2 && !send2(req)) {
      return;
    } else if (memtype != MemType::TIER1 && memtype != MemType::TIER2) {
      assert(false && "Invalid MemType");
    }

    if (epoch && epoch->policy == EPOCH_GUIDE) {
      if (req_memtype == MemType::TIER1) {
        cur_hot_pages.insert((req_addr >> 12));
      }
    }

  } else if (req_type == Request::Type::UNMAP) {

    if (memory1.pending_requests() == 0 && memory2.pending_requests() == 0) {

      auto mtit = page_memtype.find(req_addr);
      if (mtit != page_memtype.end()) {
        get_memory(mtit->second).unmap(req_addr);
        page_memtype.erase(mtit);
      }
      auto chpit = cur_hot_pages.find(req_addr);
      if (chpit != cur_hot_pages.end()) {
        cur_hot_pages.erase(chpit);
      }
      pending_unmap = false;

    } else {

      pending_unmap = true;

    }

  } else if (req_type == Request::Type::PHASE_START) {

    Config::set_cur_phase(req_addr);
    if (Params::instance()->phase_free) {
      if (memory1.pending_requests() == 0 && memory2.pending_requests() == 0) {
        invalidate_pages();
        pending_unmap = false;
      } else {
        pending_unmap = true;
        if (Config::get_prev_phase() != INVALID_PHASE &&
            Config::get_prev_phase() != Config::get_cur_phase()) {
          ++phase_changes;
        }
      }

    } else if (Params::instance()->phase_change_free) {

      if (pending_unmap &&
          memory1.pending_requests() == 0 &&
          memory2.pending_requests() == 0) {
        invalidate_pages();
        pending_unmap = false;
      } else {
        if (Config::get_prev_phase() != INVALID_PHASE &&
            Config::get_prev_phase() != Config::get_cur_phase()) {
          if (pending_unmap == false) {
            pending_unmap = true;
            Processor::set_phase_begin(true);
            ++phase_changes;
#if 0
            cout << "phase_change: " << clk << " "
                 << Config::get_cur_phase() << endl;
#endif
            cur_hot_pages.clear();
          }
        }
      }

    } else {
      if (Config::get_prev_phase() != INVALID_PHASE &&
          Config::get_prev_phase() != Config::get_cur_phase()) {
        Processor::set_phase_begin(true);
        ++phase_changes;
        //cout << "phase_change: " << clk << " "
        //     << Config::get_cur_phase() << endl;
        if (epoch && epoch->policy == EPOCH_GUIDE) {
          cur_hot_pages.clear();
        }
      }
    }

    clk--;

  } else if (req_type == Request::Type::PHASE_END) {
    Config::set_prev_phase(req_addr);
    clk--;

  } else if (req_type == Request::Type::INVALID_HOT_PAGE) {

    if (Params::instance()->phase_change_free ||
        (epoch && epoch->policy == EPOCH_GUIDE)) {
      auto page_it = page_memtype.find(req_addr);
      if (page_it != page_memtype.end()) {
        if (page_it->second == MemType::TIER1) {
          get_memory(page_it->second).mark_invalid_hot_page(req_addr);
        }
        //cout << "invalid_hot: " << req_addr << endl;
        cur_hot_pages.insert(req_addr);
      }
    }
    clk--;

  } else if (req_type == Request::Type::MIGRATE_WITH_FAULT) {

    /* We have just finished enqueuing all the commands to migrate a page --
     * update the page maps to indicate it is in the right place
     *
     * Note that we can go ahead and purge without enqueing a purge command
     * because all the relevant stats are updated before the controller
     * actually issues the commands
     */
    MemType src_memtype = (req_memtype == MemType::TIER1) ?
                           MemType::TIER2 : MemType::TIER1;

    MemoryBase& src_mem = get_memory(src_memtype);
    MemoryBase& dst_mem = get_memory(req_memtype);

    src_mem.purge(req_addr);
    dst_mem.set_page_status(req_addr, MemStatus::RESERVED);
    //cout << "mig_set_memt: " << req_addr << " "
    //     << req_memtype << endl;
    page_memtype[req_addr] = req_memtype;
    clk--;

  } else if (req_type == Request::Type::MIGRATE_SANS_FAULT) {

    MemType src_memtype = (req_memtype == MemType::TIER1) ?
                           MemType::TIER2 : MemType::TIER1;

    MemoryBase& src_mem = get_memory(src_memtype);
    MemoryBase& dst_mem = get_memory(req_memtype);

    src_mem.purge(req_addr);
    dst_mem.set_page_status(req_addr, MemStatus::VALID);
    //cout << "mig_set_memt: " << req_addr << " "
    //     << req_memtype << endl;
    page_memtype[req_addr] = req_memtype;
    clk--;

  } else if (req_type == Request::Type::SKIP) {
    clk--;
    // do nothing
  }

  if (long(cpu_inst.value()) == expected_limit_insts && !reached_limit) {
    record_cycs = clk;
    record_insts = long(cpu_inst.value());
    memory1.record_core(id);
    memory2.record_core(id);
    reached_limit = true;
  }

  if (no_core_caches) {
    more_reqs = trace.get_unfiltered_request(
        bubble_cnt, req_addr, req_type, req_memtype,
        inj_trace, pending_unmap);
    if (req_addr != -1) {
      if (req_memtype == MemType::TIER1) {
        req_addr = memory1.page_allocator(req_addr, id);
      } else if (req_memtype == MemType::TIER2) {
        req_addr = memory2.page_allocator(req_addr, id);
      }
    }
  } else {
    more_reqs = trace.get_unfiltered_request(
        bubble_cnt, req_addr, req_type, req_memtype,
        inj_trace, pending_unmap);
    if (req_addr != -1) {
      if (req_memtype == MemType::TIER1) {
        req_addr = memory1.page_allocator(req_addr, id);
      } else if (req_memtype == MemType::TIER2) {
        req_addr = memory2.page_allocator(req_addr, id);
      }
    }
  }

  if (!more_reqs) {
    /* if the length of this trace is shorter than expected length, then
     * record it when the whole trace finishes, and set reached_limit to true.
     */
    if (!reached_limit) { 
      record_cycs = clk;
      record_insts = long(cpu_inst.value());
      memory1.record_core(id);
      memory2.record_core(id);
      reached_limit = true;
    }
  }

  if (memory1.pending_fault() || memory2.pending_fault()) {
    if (Params::instance()->fault_penalty) {
      bubble_cnt += PAGE_FAULT_PENALTY;
      page_fault_inst += PAGE_FAULT_PENALTY;
      if(Config::in_valid_phase()){
        int cur_phase = Config::get_cur_phase();
        page_fault_inst_phase[cur_phase] += PAGE_FAULT_PENALTY;
      }
    }
    memory1.set_pending_fault(false);
    memory2.set_pending_fault(false);
  }
}

bool Core::finished()
{
  return !more_reqs && window.is_empty();
}

bool Core::has_reached_limit() {
  return reached_limit;
}

void Core::receive(Request& req)
{
  window.set_ready(req.addr, ~(l1_blocksz - 1l));
  if (req.arrive != -1 && req.depart > last) {
    int cur_phase = Config::get_cur_phase();
    if (req.memtype == TIER1) {
      memory_access_cycles1 += (req.depart - max(last, req.arrive));
      if (cur_phase != INVALID_PHASE) {
        memory_access_cycles1_phase[cur_phase] +=
          (req.depart - max(last, req.arrive));
      }
    } else {
      memory_access_cycles2 += (req.depart - max(last, req.arrive));
      if (cur_phase != INVALID_PHASE) {
        memory_access_cycles2_phase[cur_phase] +=
          (req.depart - max(last, req.arrive));
      }
    }
    last = req.depart;
  }
}

bool Window::is_full()
{
  return load == depth;
}

bool Window::is_empty()
{
  return load == 0;
}


void Window::insert(bool ready, long addr)
{
  assert(load <= depth);

  ready_list.at(head) = ready;
  addr_list.at(head) = addr;

  head = (head + 1) % depth;
  load++;
}


long Window::retire()
{
  assert(load <= depth);

  if (load == 0) return 0;

  int retired = 0;
  while (load > 0 && retired < ipc) {
    if (!ready_list.at(tail))
      break;

    tail = (tail + 1) % depth;
    load--;
    retired++;
  }

  return retired;
}


void Window::set_ready(long addr, int mask)
{
  if (load == 0) return;

  for (int i = 0; i < load; i++) {
    int index = (tail + i) % depth;
    if ((addr_list.at(index) & mask) != (addr & mask))
      continue;
    ready_list.at(index) = true;
  }
}



Trace::Trace(const char* trace_fname) : file(trace_fname), trace_name(trace_fname)
{
  if (!file.good()) {
    std::cerr << "Bad trace file: " << trace_fname << std::endl;
    exit(1);
  }
  trace_request_count.name("trace_request_count")
    .desc("number of requests in the trace")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  trace_request_count = 0;

  trace_t1_request_count.name("trace_t1_request_count")
    .desc("number of tier 1 requests in the trace")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  trace_t1_request_count = 0;

  trace_t2_request_count.name("trace_t2_request_count")
    .desc("number of tier 2 requests in the trace")
    .precision(0)
    .memtype(MemType::BOTH)
    ;
  trace_t2_request_count = 0;

}

bool Trace::get_unfiltered_request(long& bubble_cnt, long& req_addr,
    Request::Type& req_type, MemType& req_memtype, vector<string>& inj_trace,
    bool pending_unmap)
{
  string line;
  int mt, tc;
  TraceCommand cmd;
  stringstream ss;

  if (pending_unmap) {
    return true;
  }

  if (!inj_trace.empty()) {
    auto head = inj_trace.begin();
    line = (*head);
    inj_trace.erase(head);

  } else {
    getline(file, line);
    if (file.eof()) {
      file.clear();
      file.seekg(0, file.beg);
      return false;
    }
  }

  ss.clear(); ss.str(line);
  ss >> bubble_cnt >> mt >> req_addr >> tc;

#if 0
  cout << bubble_cnt << " " << mt << " " << req_addr << " "
       << (req_addr>>12) << " " << tc << endl;
#endif

  req_memtype = (MemType) mt;
  cmd = (TraceCommand) tc;
  if (cmd == TC_READ) {
    req_type = Request::Type::READ;
  } else if (cmd == TC_WRITE) {
    req_type = Request::Type::WRITE;
  } else if (cmd == TC_UNMAP) {
    req_type = Request::Type::UNMAP;
  } else if (cmd == TC_PHASE_START) {
    req_type = Request::Type::PHASE_START;
  } else if (cmd == TC_PHASE_END) {
    req_type = Request::Type::PHASE_END;
  } else if (cmd == TC_INVALID_HOT_PAGE) {
    req_type = Request::Type::INVALID_HOT_PAGE;
  } else if (cmd == TC_MIGRATE_WITH_FAULT) {
    req_type = Request::Type::MIGRATE_WITH_FAULT;
  } else if (cmd == TC_MIGRATE_SANS_FAULT) {
    req_type = Request::Type::MIGRATE_SANS_FAULT;
  } else {
    assert(false);
  }

  /* wait until we have processed all of the phase change commands (including
   * the invalid_hot commands) and then set pending_preload so that we do the
   * preload on the next cycle
   */
  if (Processor::at_phase_begin() &&
      (cmd != TC_PHASE_START && cmd != TC_INVALID_HOT_PAGE)) {
    if (Params::instance()->phase_change_free) {
      Processor::set_pending_preload(true);
    }
    Processor::set_phase_begin(false);
  }

  // Let's see how many requests are in the trace
  trace_request_count++;
  if (req_memtype == MemType::TIER1) {
    trace_t1_request_count++;
  } else if (req_memtype == MemType::TIER2) {
    trace_t2_request_count++;
  } 

  return true;
}

bool Trace::get_filtered_request(long& bubble_cnt, long& req_addr,
    Request::Type& req_type, MemType& memtype)
{
  static bool has_write = false;
  static long write_addr;
  static int line_num = 0;
  if (has_write){
    bubble_cnt = 0;
    req_addr = write_addr;
    req_type = Request::Type::WRITE;
    has_write = false;

    // Let's see how many requests are in the trace
    trace_request_count++;
    if (memtype == MemType::TIER1) {
      trace_t1_request_count++;
    } else if (memtype == MemType::TIER2) {
      trace_t2_request_count++;
    } 

    return true;
  }
  string line;
  size_t pos, end;

  getline(file, line);
  line_num ++;
  if (file.eof() || line.size() == 0) {
    file.clear();
    file.seekg(0, file.beg);
    has_write = false;
    line_num = 0;
    return false;
  }

  bubble_cnt = std::stoul(line, &pos, 10);

  pos = line.find_first_not_of(' ', pos+1);
  memtype = (MemType)std::stoi(line.substr(pos), &pos, 10);
  pos = line.find_first_not_of(' ', pos+1);
  req_addr = stoul(line.substr(pos), &end, 0);
  req_type = Request::Type::READ;

  pos = line.find_first_not_of(' ', pos+end);
  if (pos != string::npos){
    has_write = true;
    write_addr = stoul(line.substr(pos), NULL, 0);
  }

  // Let's see how many requests are in the trace
  trace_request_count++;
  if (memtype == MemType::TIER1) {
    trace_t1_request_count++;
  } else if (memtype == MemType::TIER2) {
    trace_t2_request_count++;
  } 

  return true;
}

bool Trace::get_dramtrace_request(long& req_addr, Request::Type& req_type, 
    MemType& req_memtype)
{
  string line;
  getline(file, line);
  if (file.eof()) {
    return false;
  }
  size_t pos;
  req_addr = std::stoul(line, &pos, 16);

  pos = line.find_first_not_of(' ', pos+1);

  req_memtype = (MemType)std::stoi(line.substr(pos), &pos, 10);
  pos = line.find_first_not_of(' ', pos+1);

  if (pos == string::npos || line.substr(pos)[0] == 'R')
    req_type = Request::Type::READ;
  else if (line.substr(pos)[0] == 'W')
    req_type = Request::Type::WRITE;
  else assert(false);
  return true;
}
