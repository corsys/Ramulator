#ifndef __MEMORY_H
#define __MEMORY_H

#include "Config.h"
#include "DRAM.h"
#include "Request.h"
#include "Controller.h"
#include "SpeedyController.h"
#include "Statistics.h"
#include "GDDR5.h"
#include "HBM.h"
#include "LPDDR3.h"
#include "LPDDR4.h"
#include "WideIO2.h"
#include "DSARP.h"
#include <vector>
#include <functional>
#include <cmath>
#include <cassert>
#include <tuple>
#include <set>
#include <climits>
#include "Params.h"

using namespace std;

namespace ramulator
{

class MemoryBase{
public:
    MemoryBase() {}
    virtual ~MemoryBase() {}
    virtual double clk_ns() = 0;
    virtual void tick() = 0;
    virtual bool send(Request req) = 0;
    virtual bool unmap(long addr) = 0;
    virtual void purge(long addr) = 0;
    virtual void mark_invalid_hot_page(long addr) = 0;
    virtual void mark_empty_slot(long addr) = 0;
    //virtual void mark_migrating(long addr) = 0;
    virtual void validate_in_pages(vector<long> page_set) = 0;
    virtual void validate_out_pages(vector<long> page_set) = 0;
    virtual void get_invalid_pages_in_set(vector<long> input, vector<long> &output) = 0;
    virtual void get_invalid_pages_out_set(vector<long> input, vector<long> &output) = 0;
    virtual void get_invalid_accessed_pages_in_set(vector<long> input, vector<long> &output) = 0;
    virtual void get_invalid_faulted_pages_in_set(vector<long> input, vector<long> &output) = 0;
    virtual unsigned int nr_free_pages() = 0;
    virtual unsigned int nr_empty_pages() = 0;
    virtual bool has_space() = 0;
    virtual bool has_empty_space() = 0;
    virtual int pending_requests() = 0;
    virtual void finish(void) = 0;
    virtual long page_allocator(long addr, int coreid) = 0;
    virtual void record_core(int coreid) = 0;
    virtual bool pending_fault() = 0;
    virtual void set_pending_fault(bool f) = 0;
    virtual void invalidate_pages() = 0;
    virtual MemStatus page_status(long pg) = 0;
    virtual void set_page_status(long pg, MemStatus st) = 0;
    virtual bool has_page_status(long pg) = 0;
    virtual long get_victim() = 0;
    virtual long size() = 0;
    virtual multimap<long, long>::iterator page_counts_begin() = 0;
    virtual multimap<long, long>::iterator page_counts_end() = 0;
    virtual void reset_page_counts() = 0;
    virtual void reset_page_faults() = 0;
    virtual void reset_page_migrate_status() = 0;
};

template <class T, template<typename> class Controller = Controller >
class Memory : public MemoryBase
{
protected:
  ScalarStat dram_capacity;
  ScalarStat num_dram_cycles;
  ScalarStat num_incoming_requests;
  VectorStat num_read_requests;
  VectorStat num_write_requests;
  ScalarStat ramulator_active_cycles;
  VectorStat incoming_requests_per_channel;
  VectorStat incoming_read_reqs_per_channel;

  ScalarStat physical_page_replacement;
  ScalarStat maximum_bandwidth;
  ScalarStat in_queue_req_num_sum;
  ScalarStat in_queue_read_req_num_sum;
  ScalarStat in_queue_write_req_num_sum;
  ScalarStat in_queue_req_num_avg;
  ScalarStat in_queue_read_req_num_avg;
  ScalarStat in_queue_write_req_num_avg;

  ScalarStat mt_size;
  ScalarStat mt_peak_size;
  ScalarStat mt_faults;
  ScalarStat mt_accs;
  ScalarStat mt_unmaps;
  ScalarStat mt_purges;
  ScalarStat mt_invalid_hots;
  VectorStat mt_req_accs;
  VectorStat mt_req_faults;

  VectorStat mt_size_phase;
  VectorStat mt_peak_size_phase;
  VectorStat mt_faults_phase;
  VectorStat mt_accs_phase;

  VectorStat mt_req_accs_t1_phase;
  VectorStat mt_req_accs_t2_phase;
  VectorStat mt_req_faults_t1_phase;
  VectorStat mt_req_faults_t2_phase;

  map<long, MemStatus> pages;
  unsigned int tier_limit;
  bool _pending_fault;

#ifndef INTEGRATED_WITH_GEM5
  VectorStat record_read_requests;
  VectorStat record_write_requests;
#endif

  long max_address;
  map<long, long> page_counts;
  set<long> page_faults;
public:
    enum class Type {
        ChRaBaRoCo,
        RoBaRaCoCh,
        MAX,
    } type = Type::RoBaRaCoCh;

    enum class Translation {
      None,
      Random,
      MAX,
    } translation = Translation::None;

    std::map<string, Translation> name_to_translation = {
      {"None", Translation::None},
      {"Random", Translation::Random},
    };

    multimap<long, long>::iterator  page_counts_begin(){
      return page_counts.begin();
    }
    multimap<long, long>::iterator  page_counts_end(){
      return page_counts.end();
    }

    vector<int> free_physical_pages;
    long free_physical_pages_remaining;
    map<pair<int, long>, long> page_translation;

    vector<Controller<T>*> ctrls;
    T * spec;
    vector<int> addr_bits;
    MemType my_mem_type;

    int tx_bits;

    Memory(const Config& configs, vector<Controller<T>*> ctrls, unsigned int limit = UINT_MAX)
        : ctrls(ctrls),
          spec(ctrls[0]->channel->spec),
          addr_bits(int(T::Level::MAX))
    {
        // make sure 2^N channels/ranks
        // TODO support channel number that is not powers of 2
        int *sz = spec->org_entry.count;
        assert((sz[0] & (sz[0] - 1)) == 0);
        assert((sz[1] & (sz[1] - 1)) == 0);
        // validate size of one transaction
        int tx = (spec->prefetch_size * spec->channel_width / 8);
        tx_bits = calc_log2(tx);
        tier_limit = limit;
        assert((1<<tx_bits) == tx);
        // If hi address bits will not be assigned to Rows
        // then the chips must not be LPDDRx 6Gb, 12Gb etc.
        if (type != Type::RoBaRaCoCh && spec->standard_name.substr(0, 5) == "LPDDR")
            assert((sz[int(T::Level::Row)] & (sz[int(T::Level::Row)] - 1)) == 0);

        max_address = spec->channel_width / 8;

        for (unsigned int lev = 0; lev < addr_bits.size(); lev++) {
          addr_bits[lev] = calc_log2(sz[lev]);
            max_address *= sz[lev];
        }

        addr_bits[int(T::Level::MAX) - 1] -= calc_log2(spec->prefetch_size);

        // Initiating translation
        if (configs.contains("translation")) {
          translation = name_to_translation[configs["translation"]];
        }
        if (translation != Translation::None) {
          // construct a list of available pages
          // TODO: this should not assume a 4KB page!
          free_physical_pages_remaining = max_address >> 12;

          free_physical_pages.resize(free_physical_pages_remaining, -1);
        }

        my_mem_type = configs.get_mem_type();

        dram_capacity
            .name("dram_capacity")
            .desc("Number of bytes in simulated DRAM")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        dram_capacity = max_address;

        num_dram_cycles
            .name("dram_cycles")
            .desc("Number of DRAM cycles simulated")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        num_incoming_requests
            .name("incoming_requests")
            .desc("Number of incoming requests to DRAM")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        num_read_requests
            .init(configs.get_core_num())
            .name("read_requests")
            .desc("Number of incoming read requests to DRAM per core")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        num_write_requests
            .init(configs.get_core_num())
            .name("write_requests")
            .desc("Number of incoming write requests to DRAM per core")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        incoming_requests_per_channel
            .init(sz[int(T::Level::Channel)])
            .name("incoming_requests_per_channel")
            .desc("Number of incoming requests to each DRAM channel")
            .memtype(configs.get_mem_type())
            ;
        incoming_read_reqs_per_channel
            .init(sz[int(T::Level::Channel)])
            .name("incoming_read_reqs_per_channel")
            .desc("Number of incoming read requests to each DRAM channel")
            .memtype(configs.get_mem_type())
            ;

        ramulator_active_cycles
            .name("ramulator_active_cycles")
            .desc("The total number of cycles that the DRAM part is active (serving R/W)")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        physical_page_replacement
            .name("physical_page_replacement")
            .desc("The number of times that physical page replacement happens.")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        maximum_bandwidth
            .name("maximum_bandwidth")
            .desc("The theoretical maximum bandwidth (Bps)")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        in_queue_req_num_sum
            .name("in_queue_req_num_sum")
            .desc("Sum of read/write queue length")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        in_queue_read_req_num_sum
            .name("in_queue_read_req_num_sum")
            .desc("Sum of read queue length")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        in_queue_write_req_num_sum
            .name("in_queue_write_req_num_sum")
            .desc("Sum of write queue length")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        in_queue_req_num_avg
            .name("in_queue_req_num_avg")
            .desc("Average of read/write queue length per memory cycle")
            .precision(6)
            .memtype(configs.get_mem_type())
            ;
        in_queue_read_req_num_avg
            .name("in_queue_read_req_num_avg")
            .desc("Average of read queue length per memory cycle")
            .precision(6)
            .memtype(configs.get_mem_type())
            ;
        in_queue_write_req_num_avg
            .name("in_queue_write_req_num_avg")
            .desc("Average of write queue length per memory cycle")
            .precision(6)
            .memtype(configs.get_mem_type())
            ;

        mt_size
            .name("memory_tier_size")
            .desc("Size of memory tier")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_peak_size
            .name("memory_tier_peak_size")
            .desc("Peak memory size")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_faults
            .name("memory_tier_faults")
            .desc("Faults to memory tier")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_purges
            .name("memory_tier_purges")
            .desc("Purges from memory tier (for migration)")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_invalid_hots
            .name("memory_tier_invalid_hots")
            .desc("Pages that are invalid, but marked as hot")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_accs
            .name("memory_tier_accs")
            .desc("Accs on memory tier")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_unmaps
            .name("memory_tier_unmaps")
            .desc("Unmap requests to memory tier")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_req_accs
            .init(2)
            .name("memory_tier_req_accs")
            .desc("Accs that were requested per tier")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_req_faults
            .init(2)
            .name("memory_tier_req_faults")
            .desc("Faults that were requested per tier")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;

        mt_size_phase
            .init(MAX_PHASES)
            .name("memory_tier_size_phase")
            .desc("Size of memory tier")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_peak_size_phase
            .init(MAX_PHASES)
            .name("memory_tier_peak_size_phase")
            .desc("Peak memory size")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_faults_phase
            .init(MAX_PHASES)
            .name("memory_tier_faults_phase")
            .desc("Faults to memory tier")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_accs_phase
            .init(MAX_PHASES)
            .name("memory_tier_accs_phase")
            .desc("Accs on memory tier")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;

        mt_req_accs_t1_phase
            .init(MAX_PHASES)
            .name("mt_req_accs_t1_phase")
            .desc("Accs that were requested by t1")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_req_accs_t2_phase
            .init(MAX_PHASES)
            .name("mt_req_accs_t2_phase")
            .desc("Accs that were requested by t2")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_req_faults_t1_phase
            .init(MAX_PHASES)
            .name("mt_req_faults_t1_phase")
            .desc("Faults that were the result of requests to t1")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;
        mt_req_faults_t2_phase
            .init(MAX_PHASES)
            .name("mt_req_faults_t2_phase")
            .desc("Faults that were the result of requests to t2")
            .precision(0)
            .memtype(configs.get_mem_type())
            ;

#ifndef INTEGRATED_WITH_GEM5
        record_read_requests
            .init(configs.get_core_num())
            .name("record_read_requests")
            .desc("record read requests for this core when it reaches request limit or to the end")
            .memtype(configs.get_mem_type())
            ;

        record_write_requests
            .init(configs.get_core_num())
            .name("record_write_requests")
            .desc("record write requests for this core when it reaches request limit or to the end")
            .memtype(configs.get_mem_type())
            ;
#endif

    }

    ~Memory()
    {
        for (auto ctrl: ctrls)
            delete ctrl;
        delete spec;
    }

    double clk_ns()
    {
        return spec->speed_entry.tCK;
    }

    void record_core(int coreid) {
#ifndef INTEGRATED_WITH_GEM5
      record_read_requests[coreid] = num_read_requests[coreid];
      record_write_requests[coreid] = num_write_requests[coreid];
#endif
      for (auto ctrl : ctrls) {
        ctrl->record_core(coreid);
      }
    }

    void tick()
    {
        ++num_dram_cycles;
        int cur_que_req_num = 0;
        int cur_que_readreq_num = 0;
        int cur_que_writereq_num = 0;
        for (auto ctrl : ctrls) {
          cur_que_req_num += ctrl->readq.size() + ctrl->writeq.size() + ctrl->pending.size();
          cur_que_readreq_num += ctrl->readq.size() + ctrl->pending.size();
          cur_que_writereq_num += ctrl->writeq.size();
        }
        in_queue_req_num_sum += cur_que_req_num;
        in_queue_read_req_num_sum += cur_que_readreq_num;
        in_queue_write_req_num_sum += cur_que_writereq_num;

        bool is_active = false;
        for (auto ctrl : ctrls) {
          is_active = is_active || ctrl->is_active();
          ctrl->tick();
        }
        if (is_active) {
          ramulator_active_cycles++;
        }
    }

    long size() { return pages.size(); }

    void recompute_size() {
      mt_size = pages.size();
      mt_peak_size = (mt_size.value() > mt_peak_size.value()) ?
                      mt_size.value() : mt_peak_size.value();

      int cur_phase = Config::get_cur_phase();
      if (cur_phase != INVALID_PHASE) {
        mt_size_phase[cur_phase] = pages.size();
        mt_peak_size_phase[cur_phase] =
          (mt_size_phase[cur_phase].value() > mt_peak_size_phase[cur_phase].value()) ?
           mt_size_phase[cur_phase].value() : mt_peak_size_phase[cur_phase].value();
      }

      //cout << "size: " << mt_size.value() << endl;
      assert(mt_size.value() <= tier_limit);
    }

    bool unmap(long addr)
    {
      auto pages_it = pages.find(addr);
      if (pages_it != pages.end()) {
        pages.erase(pages_it);
        recompute_size();
        mt_unmaps++;
      }
      //cout << "unmap: " << addr << endl;
      auto page_counts_it = page_counts.find(addr);
      if (page_counts_it != page_counts.end()) {
        page_counts.erase(page_counts_it);
      }
      page_faults.erase(addr);
      return true;
    }

    void purge(long addr)
    {
      auto it = pages.find(addr);
      if (it != pages.end()) {
        pages.erase(it);
        recompute_size();
        mt_purges++;
      }
      //cout << "purge: " << addr << endl;
      auto page_counts_it = page_counts.find(addr);
      if (page_counts_it != page_counts.end()) {
        page_counts.erase(page_counts_it);
      }
      page_faults.erase(addr);
    }

    void mark_invalid_hot_page(long addr)
    {
      auto it = pages.find(addr);
      if (it != pages.end() && (pages[addr] == MemStatus::INVALID)) {
        pages[addr] = MemStatus::INVALID_HOT;
        mt_invalid_hots++;
      }
    }

#if 0
    void mark_migrating(long addr)
    {
      auto it = pages.find(addr);
      assert( it != pages.end() &&
              ( (pages[addr] == MemStatus::INVALID) ||
                (pages[addr] == MemStatus::INVALID_HOT) ) );
      pages[addr] = MemStatus::MIGRATING;
    }
#endif

    void mark_empty_slot(long addr)
    {
      pages[addr] = MemStatus::EMPTY_RESERVED;
    }

    void reset_page_migrate_status() {
      for (auto page_it = pages.begin(); page_it != pages.end(); page_it++) {
        if (page_it->second == MemStatus::EMPTY_RESERVED) {
          pages.erase(page_it);
        }
      }
    }

    /* for each page in the page_set, if the page is in this memory, set the
     * page's status to VALID
     */
    void validate_in_pages(vector<long> page_set)
    {
      for(auto page_set_it = page_set.begin();
          page_set_it != page_set.end();
          ++page_set_it) {
        auto pages_it = pages.find(*page_set_it);
        if ( pages_it != pages.end() &&
             ( (pages[*page_set_it] == INVALID) ||
               (pages[*page_set_it] == INVALID_HOT) ) ) {
          pages[*page_set_it] = MemStatus::VALID;
        }
      }
    }

    /* for each page in this memory, if the page is not in the page_set, set
     * the page's status to VALID
     */
    void validate_out_pages(vector<long> page_set)
    {
      for(auto pages_it = pages.begin();
          pages_it != pages.end();
          ++pages_it) {
        auto page_set_it = find(page_set.begin(), page_set.end(),
                                pages_it->first);
        if (page_set_it == page_set.end() &&
            ( (pages[pages_it->first] == INVALID) ||
              (pages[pages_it->first] == INVALID_HOT) ) ) {
          pages[pages_it->first] = MemStatus::VALID;
        }
      }
    }

    void get_invalid_faulted_pages_in_set(vector<long> input,
      vector<long>& output)
    {
      for(auto input_it = input.begin();
          input_it != input.end();
          ++input_it) {
        auto pages_it = pages.find(*input_it);
        if (pages_it != pages.end() &&
            ( page_faults.find(*input_it) != page_faults.end() ) &&
            ( pages[*input_it] == MemStatus::INVALID ||
              pages[*input_it] == MemStatus::INVALID_HOT ) ) {
          output.push_back(*input_it);
        }
      }
    }

    void get_invalid_accessed_pages_in_set(vector<long> input,
      vector<long>& output)
    {
      for(auto input_it = input.begin();
          input_it != input.end();
          ++input_it) {
        auto pages_it = pages.find(*input_it);
        if (pages_it != pages.end() &&
            ( page_counts[*input_it] > 0 ) &&
            ( pages[*input_it] == MemStatus::INVALID ||
              pages[*input_it] == MemStatus::INVALID_HOT ) ) {
          output.push_back(*input_it);
        }
      }
    }

    void get_invalid_pages_in_set(vector<long> input,
      vector<long>& output)
    {
      for(auto input_it = input.begin();
          input_it != input.end();
          ++input_it) {
        auto pages_it = pages.find(*input_it);
        if (pages_it != pages.end() &&
            ( pages[*input_it] == MemStatus::INVALID ||
              pages[*input_it] == MemStatus::INVALID_HOT ) ) {
          output.push_back(*input_it);
        }
      }
    }

    void get_invalid_pages_out_set(vector<long> input,
      vector<long>& output)
    {
      for(auto pages_it = pages.begin();
          pages_it != pages.end();
          ++pages_it) {
        auto input_it = find(input.begin(), input.end(), pages_it->first);
        if ( input_it == input.end() &&
             (pages_it->second == MemStatus::INVALID ||
              pages_it->second == MemStatus::INVALID_HOT) ) {
          output.push_back(pages_it->first);
        }
      }
    }

    bool send(Request req)
    {
        req.addr_vec.resize(addr_bits.size());
        long addr = req.addr;
        int coreid = req.coreid;
        // Each transaction size is 2^tx_bits, so first clear the lowest tx_bits bits
        clear_lower_bits(addr, tx_bits);
        switch(int(type)){
            case int(Type::ChRaBaRoCo):
                for (int i = addr_bits.size() - 1; i >= 0; i--)
                    req.addr_vec[i] = slice_lower_bits(addr, addr_bits[i]);
                break;
            case int(Type::RoBaRaCoCh):
                req.addr_vec[0] = slice_lower_bits(addr, addr_bits[0]);
                req.addr_vec[addr_bits.size() - 1] = slice_lower_bits(addr, addr_bits[addr_bits.size() - 1]);
                for (int i = 1; i <= int(T::Level::Row); i++)
                    req.addr_vec[i] = slice_lower_bits(addr, addr_bits[i]);
                break;
            default:
                assert(false);
        }
        addr = req.addr >> 12;

        int cur_phase = Config::get_cur_phase();
        if(ctrls[req.addr_vec[0]]->enqueue(req)) {
            // tally stats here to avoid double counting for requests that aren't enqueued
            
            ++num_incoming_requests;
            ++mt_accs;
            if (cur_phase != INVALID_PHASE) {
              ++mt_accs_phase[cur_phase];
            }

            if(req.req_memtype == MemType::TIER1 || req.req_memtype == MemType::TIER2) {
              ++mt_req_accs[req.req_memtype];
              if (cur_phase != INVALID_PHASE) {
                if (req.req_memtype == MemType::TIER1) {
                  ++mt_req_accs_t1_phase[cur_phase];
                } else {
                  ++mt_req_accs_t2_phase[cur_phase];
                }
              }
            }

            assert(pages.find(addr) != pages.end());
            assert(page_counts.find(addr) != page_counts.end());
            MemStatus status = pages[addr];
            assert(status != INVALID);

            page_counts[addr] += 1; 

            if (status == MemStatus::RESERVED) {
              _pending_fault = true;
              ++mt_faults;
              if (cur_phase != INVALID_PHASE) {
                ++mt_faults_phase[cur_phase];
              }
              if(req.req_memtype == MemType::TIER1 || req.req_memtype == MemType::TIER2) {
                ++mt_req_faults[req.req_memtype];
                if (cur_phase != INVALID_PHASE) {
                  if (req.req_memtype == MemType::TIER1) {
                    ++mt_req_faults_t1_phase[cur_phase];
                  } else {
                    ++mt_req_faults_t2_phase[cur_phase];
                  }
                }
              }
              pages[addr] = MemStatus::VALID;
              page_faults.insert(addr);
            }

            recompute_size();

            if (req.type == Request::Type::READ) {
              ++num_read_requests[coreid];
              ++incoming_read_reqs_per_channel[req.addr_vec[int(T::Level::Channel)]];
            }
            if (req.type == Request::Type::WRITE) {
              ++num_write_requests[coreid];
            }
            ++incoming_requests_per_channel[req.addr_vec[int(T::Level::Channel)]];
            return true;
        }

        return false;
    }

    int pending_requests()
    {
        int reqs = 0;
        for (auto ctrl: ctrls)
            reqs += ctrl->readq.size() + ctrl->writeq.size() + ctrl->otherq.size() + ctrl->pending.size();
        return reqs;
    }

    bool pending_fault()           { return _pending_fault; }
    void set_pending_fault(bool f) { _pending_fault = f;    }


    void finish(void) {
      dram_capacity = max_address;
      int *sz = spec->org_entry.count;
      maximum_bandwidth = spec->speed_entry.rate * 1e6 * spec->channel_width * sz[int(T::Level::Channel)] / 8;
      long dram_cycles = num_dram_cycles.value();
      for (auto ctrl : ctrls) {
        long read_req = long(incoming_read_reqs_per_channel[ctrl->channel->id].value());
        ctrl->finish(read_req, dram_cycles);
      }


      // finalize average queueing requests
      in_queue_req_num_avg = in_queue_req_num_sum.value() / dram_cycles;
      in_queue_read_req_num_avg = in_queue_read_req_num_sum.value() / dram_cycles;
      in_queue_write_req_num_avg = in_queue_write_req_num_sum.value() / dram_cycles;
    }

    long page_allocator(long addr, int coreid) {
        long virtual_page_number = addr >> 12;

        switch(int(translation)) {
            case int(Translation::None): {
              return addr;
            }
            case int(Translation::Random): {
                auto target = make_pair(coreid, virtual_page_number);
                if(page_translation.find(target) == page_translation.end()) {
                    // page doesn't exist, so assign a new page
                    // make sure there are physical pages left to be assigned

                    // if physical page doesn't remain, replace a previous assigned
                    // physical page.
                    if (!free_physical_pages_remaining) {
                      physical_page_replacement++;
                      long phys_page_to_read = lrand() % free_physical_pages.size();
                      assert(free_physical_pages[phys_page_to_read] != -1);
                      page_translation[target] = phys_page_to_read;
                    } else {
                        // assign a new page
                        long phys_page_to_read = lrand() % free_physical_pages.size();
                        // if the randomly-selected page was already assigned
                        if(free_physical_pages[phys_page_to_read] != -1) {
                            long starting_page_of_search = phys_page_to_read;

                            do {
                                // iterate through the list until we find a free page
                                // TODO: does this introduce serious non-randomness?
                                ++phys_page_to_read;
                                phys_page_to_read %= free_physical_pages.size();
                            }
                            while((phys_page_to_read != starting_page_of_search) && free_physical_pages[phys_page_to_read] != -1);
                        }

                        assert(free_physical_pages[phys_page_to_read] == -1);

                        page_translation[target] = phys_page_to_read;
                        free_physical_pages[phys_page_to_read] = coreid;
                        --free_physical_pages_remaining;
                    }
                }

                // SAUGATA TODO: page size should not always be fixed to 4KB
                return (page_translation[target] << 12) | (addr & ((1 << 12) - 1));
            }
            default:
                assert(false);
        }

    }

    unsigned int nr_empty_pages() {
      return (tier_limit - pages.size());
    }

    unsigned int nr_free_pages() {
      int free = 0;

      free += nr_empty_pages();
      for (auto it = pages.begin(); it != pages.end(); ++it) {
        if (it->second == MemStatus::INVALID ||
            it->second == MemStatus::INVALID_HOT) {
          free++;
        }
      }
      return free;
    }

    bool has_space() {

      bool has_invalid_page = false;
      for (auto it = pages.begin(); it != pages.end(); ++it) {
        if (it->second == MemStatus::INVALID ||
            it->second == MemStatus::INVALID_HOT) {
          has_invalid_page = true;
          break;
        }
      }

      return (has_empty_space() || has_invalid_page);
    }

    bool has_empty_space(){
      return (pages.size() < tier_limit);
    }

    void invalidate_pages() {
      for (auto it = pages.begin(); it != pages.end(); it++) {
        //cout << " invalidate: " << my_mem_type << " " << it->first << endl;
        it->second = MemStatus::INVALID;
      }
    }

    void reset_page_counts(){
      for (auto page_it = page_counts.begin();
                page_it != page_counts.end();
                page_it++) {
        page_it->second = 0;
      }
    }

    void reset_page_faults(){
      page_faults.clear();
    }

    long history_guide_get_victim() {
      vector<long> invalid_candidates;
      vector<long> invalid_unfaulted_candidates;
      vector<long> invalid_unaccessed_candidates;
      vector<long> invalid_hot_candidates;
      vector<long> invalid_hot_unfaulted_candidates;
      vector<long> invalid_hot_unaccessed_candidates;

      /* prefer a page that is INVALID and was not faulted or accessed in the
       * previous interval
       */
      for (auto page_it = pages.begin();
           page_it != pages.end(); page_it++) {
        if (page_it->second == MemStatus::INVALID) {
          if ( (page_counts[page_it->first] == 0) &&
               (page_faults.find(page_it->first) == page_faults.end() ) ) {
            invalid_unaccessed_candidates.push_back(page_it->first);
          } else if ( (page_faults.find(page_it->first) == page_faults.end() ) ) {
            invalid_unfaulted_candidates.push_back(page_it->first);
          } else {
            invalid_candidates.push_back(page_it->first);
          }
        } else if (page_it->second == MemStatus::INVALID_HOT) {
          if ( (page_counts[page_it->first] == 0) &&
               (page_faults.find(page_it->first) == page_faults.end() ) ) {
            invalid_hot_unaccessed_candidates.push_back(page_it->first);
          } else if ( (page_faults.find(page_it->first) == page_faults.end() ) ) {
            invalid_hot_unfaulted_candidates.push_back(page_it->first);
          } else {
            invalid_hot_candidates.push_back(page_it->first);
          }
        }
      }

      if ( !(invalid_unaccessed_candidates.empty()) ) {
        return invalid_unaccessed_candidates[(rand() % invalid_unaccessed_candidates.size())];

      } else if ( !(invalid_unfaulted_candidates.empty()) ) {
        return invalid_unfaulted_candidates[(rand() % invalid_unfaulted_candidates.size())];

      } else if ( !(invalid_candidates.empty()) ) {
        return invalid_candidates[(rand() % invalid_candidates.size())];

      } else if ( !(invalid_hot_unaccessed_candidates.empty()) ) {
        return invalid_hot_unaccessed_candidates[(rand() % invalid_hot_unaccessed_candidates.size())];

      } else if ( !(invalid_hot_unfaulted_candidates.empty()) ) {
        return invalid_hot_unfaulted_candidates[(rand() % invalid_hot_unfaulted_candidates.size())];

      } else if ( !(invalid_hot_candidates.empty()) ) {
        return invalid_hot_candidates[(rand() % invalid_hot_candidates.size())];
      }

      /* else no victims available */
      return 0x0;
    }

    long get_victim() {
      if (Params::instance()->history_guide) {
        return history_guide_get_victim();
      }

      vector<long> candidates;

      /* return an INVALID page if one exists */
      for (auto page_it = pages.begin();
           page_it != pages.end(); page_it++) {
        if (page_it->second == MemStatus::INVALID) {
          candidates.push_back(page_it->first);
        }
      }

      if ( !(candidates.empty()) ) {
        return candidates[(rand() % candidates.size())];
      }

      /* else return an INVALID_HOT page if one exists */
      for (auto page_it = pages.begin();
           page_it != pages.end(); page_it++) {
        if (page_it->second == MemStatus::INVALID_HOT) {
          candidates.push_back(page_it->first);
        }
      }

      if ( !(candidates.empty()) ) {
        return candidates[(rand() % candidates.size())];
      }

      /* else no victims available */
      return 0x0;
    }

    MemStatus page_status(long addr) {
      auto it = pages.find(addr);
      if (it == pages.end()) {
        cout << "unknown page! " << addr << endl;
        assert(false);
      }
      //assert(it != pages.end());
      return pages[addr];
    }

    bool has_page_status(long addr) {
      auto it = pages.find(addr);
      if (it != pages.end()) {
        return true;
      }
      return false;
    }

    void set_page_status(long addr, MemStatus status) {

      /* if we haven't seen this page -- initialize its page count */
      if(page_counts.find(addr) == page_counts.end()) {
        //cout << " init_count: " << addr << " " << my_mem_type << endl;
        page_counts[addr] = 0;
      }
      /*
      if (page_counts.find(addr) == page_counts.end()) {
        cout << "status: " << addr << " " << my_mem_type << endl;
      }
      */

      pages[addr] = status;
    }

private:

    int calc_log2(int val){
        int n = 0;
        while ((val >>= 1))
            n ++;
        return n;
    }
    int slice_lower_bits(long& addr, int bits)
    {
        int lbits = addr & ((1<<bits) - 1);
        addr >>= bits;
        return lbits;
    }
    void clear_lower_bits(long& addr, int bits)
    {
        addr >>= bits;
    }
    long lrand(void) {
        if(sizeof(int) < sizeof(long)) {
            return static_cast<long>(rand()) << (sizeof(int) * 8) | rand();
        }

        return rand();
    }
};

} /*namespace ramulator*/

#endif /*__MEMORY_H*/
